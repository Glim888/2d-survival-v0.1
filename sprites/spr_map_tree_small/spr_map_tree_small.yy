{
    "id": "3b4cee10-b7aa-462b-826f-201d230681a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_tree_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 1,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7de64938-3768-4d5c-b687-fafdab26958e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "fb4f7298-d9c5-4223-b183-f31103602ecd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7de64938-3768-4d5c-b687-fafdab26958e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e67f084f-fdbb-4e4e-864b-5d6640b456a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7de64938-3768-4d5c-b687-fafdab26958e",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        },
        {
            "id": "4a988409-f442-4aa2-96b1-38a154dbd419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "7985189b-3864-474c-a3cb-ab42f34e3d70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a988409-f442-4aa2-96b1-38a154dbd419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dc419a9-dad5-4dec-a42b-ff62fe0f1db7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a988409-f442-4aa2-96b1-38a154dbd419",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        },
        {
            "id": "36bc255e-f886-470b-8eca-20c6dcf3bba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "f9e172da-d585-4399-8cb3-7733bdbca109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36bc255e-f886-470b-8eca-20c6dcf3bba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a337107-e400-48f5-b6b0-ad8581c0389a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36bc255e-f886-470b-8eca-20c6dcf3bba3",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        },
        {
            "id": "03ce6d94-c27c-4562-a519-a9046368c05b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "ea51e727-929d-4cda-be07-ff9f92bc9112",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ce6d94-c27c-4562-a519-a9046368c05b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c702643-d0f6-4da8-9cd4-9a35a02ac4eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ce6d94-c27c-4562-a519-a9046368c05b",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        },
        {
            "id": "c1944675-10f6-4c9e-b07c-3a8f35115fc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "95b03763-6bba-4c0d-b1fd-12598ba57c0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1944675-10f6-4c9e-b07c-3a8f35115fc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71337ad2-cf75-4e2d-8251-ed188ec87d17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1944675-10f6-4c9e-b07c-3a8f35115fc3",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        },
        {
            "id": "e0548776-27ba-4fac-b397-ac748884c6d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "b4830d65-14b0-4d68-b132-fa9725ed541e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0548776-27ba-4fac-b397-ac748884c6d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07b12b6f-ebb0-4c66-ba3b-df2494a8405d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0548776-27ba-4fac-b397-ac748884c6d9",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        },
        {
            "id": "1d4da42d-89c0-4abb-965d-ae796bdc3b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "aedbf9ee-8a72-433f-a1a7-8b613ee7cab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d4da42d-89c0-4abb-965d-ae796bdc3b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b5da0f9-35b7-4d38-9c9e-06053b625ebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d4da42d-89c0-4abb-965d-ae796bdc3b57",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        },
        {
            "id": "8aba24b8-1a9d-41e2-a771-6d0da8a5a1bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "317a1fd7-c02d-4df1-ba88-86a6eb4cb8a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aba24b8-1a9d-41e2-a771-6d0da8a5a1bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da88cba0-b433-4b19-a00c-6a6f08d0e7ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aba24b8-1a9d-41e2-a771-6d0da8a5a1bc",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        },
        {
            "id": "715e1484-d5b7-4bc8-a790-aebf52ce78ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "1a3cce14-cb8a-4c26-a28a-de72ff085a5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "715e1484-d5b7-4bc8-a790-aebf52ce78ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "479815a4-8297-4480-88b1-6e78e340c36b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "715e1484-d5b7-4bc8-a790-aebf52ce78ce",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        },
        {
            "id": "99483bf7-50a4-46a7-a1a0-312558ced4fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "0f7b0d05-5d1f-4894-8a21-44ed79c370b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99483bf7-50a4-46a7-a1a0-312558ced4fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed24673b-1a8e-4162-957c-0121cea89f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99483bf7-50a4-46a7-a1a0-312558ced4fa",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        },
        {
            "id": "22e05e1b-8a34-4b4f-adc0-7431489f960f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "c1fa9732-f755-4f95-b206-1ace02c0fad4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e05e1b-8a34-4b4f-adc0-7431489f960f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bc908a8-f498-4b65-993c-49448a9cb5dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e05e1b-8a34-4b4f-adc0-7431489f960f",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        },
        {
            "id": "f0c35c5a-3e97-460b-886f-8863140e83e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "compositeImage": {
                "id": "bede91b8-66f7-434c-aa54-20f2293910e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c35c5a-3e97-460b-886f-8863140e83e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6ad0f11-df5c-4b9d-afe6-64b635944262",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c35c5a-3e97-460b-886f-8863140e83e2",
                    "LayerId": "f67b95a4-b862-4e6c-a7c5-de301a1f634e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "f67b95a4-b862-4e6c-a7c5-de301a1f634e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b4cee10-b7aa-462b-826f-201d230681a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 47
}