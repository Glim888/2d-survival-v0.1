{
    "id": "28e6d080-dc11-43d4-9325-d5e02ccf81f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_stone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 2,
    "bbox_right": 38,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8657530-3b15-402e-9aa3-353fb9422639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28e6d080-dc11-43d4-9325-d5e02ccf81f8",
            "compositeImage": {
                "id": "30dc2157-208e-4a59-a3ea-58c7041f9a8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8657530-3b15-402e-9aa3-353fb9422639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d7ac354-351a-410a-91ed-c047813854ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8657530-3b15-402e-9aa3-353fb9422639",
                    "LayerId": "8c570a23-ff45-4480-b0ca-43a3421c68fa"
                }
            ]
        },
        {
            "id": "c161bce1-2d57-4b81-9fc2-4c63ca7c4945",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28e6d080-dc11-43d4-9325-d5e02ccf81f8",
            "compositeImage": {
                "id": "1e94be17-9a98-41d7-ad6e-f5c10f02106b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c161bce1-2d57-4b81-9fc2-4c63ca7c4945",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2691b4a5-a71b-4771-ac13-c1c52ec19b8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c161bce1-2d57-4b81-9fc2-4c63ca7c4945",
                    "LayerId": "8c570a23-ff45-4480-b0ca-43a3421c68fa"
                }
            ]
        },
        {
            "id": "3fa564e6-c715-4520-8b00-295732e17ade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28e6d080-dc11-43d4-9325-d5e02ccf81f8",
            "compositeImage": {
                "id": "8b659674-a491-4fd4-b625-4d0a91fbefd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa564e6-c715-4520-8b00-295732e17ade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3996e006-51ee-4e25-b251-0fa120cda8ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa564e6-c715-4520-8b00-295732e17ade",
                    "LayerId": "8c570a23-ff45-4480-b0ca-43a3421c68fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "8c570a23-ff45-4480-b0ca-43a3421c68fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28e6d080-dc11-43d4-9325-d5e02ccf81f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}