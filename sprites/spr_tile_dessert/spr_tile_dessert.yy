{
    "id": "e3484557-d543-451d-863a-6b907556148a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_dessert",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09aa23d1-c4d8-4c74-9f01-682850d74503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3484557-d543-451d-863a-6b907556148a",
            "compositeImage": {
                "id": "94d07b7d-97b2-4576-a7e3-88e236db4450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09aa23d1-c4d8-4c74-9f01-682850d74503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d52e812c-96ec-4a96-9810-2ce42dd0b4df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09aa23d1-c4d8-4c74-9f01-682850d74503",
                    "LayerId": "a2f586ad-bf3b-44b7-8d11-3b20a5f5e9a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a2f586ad-bf3b-44b7-8d11-3b20a5f5e9a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3484557-d543-451d-863a-6b907556148a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "27777447-a745-496f-8cba-7c522e2a58d4",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}