{
    "id": "37b3b8b9-1da2-4efc-80f1-eb6a20d91ae9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_example_tile_stone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1796c590-09dd-4800-9d2a-ad713996d4c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b3b8b9-1da2-4efc-80f1-eb6a20d91ae9",
            "compositeImage": {
                "id": "97a4dafc-5f5d-4cb4-913e-dda8b0267598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1796c590-09dd-4800-9d2a-ad713996d4c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6df8cf9a-a770-42ed-af38-26fabd38f1b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1796c590-09dd-4800-9d2a-ad713996d4c5",
                    "LayerId": "624632d8-dbc0-4b71-8d05-f650128ece73"
                }
            ]
        },
        {
            "id": "680543c6-ff99-45dd-b120-062edebfa830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b3b8b9-1da2-4efc-80f1-eb6a20d91ae9",
            "compositeImage": {
                "id": "1ded33b9-206e-48f9-a882-2cc4a3a7584e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "680543c6-ff99-45dd-b120-062edebfa830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6826c33-b212-4805-a7bb-f9387b4b365d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "680543c6-ff99-45dd-b120-062edebfa830",
                    "LayerId": "624632d8-dbc0-4b71-8d05-f650128ece73"
                }
            ]
        },
        {
            "id": "99fe58ea-f58e-44e2-8870-91b5118ef28a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b3b8b9-1da2-4efc-80f1-eb6a20d91ae9",
            "compositeImage": {
                "id": "f43fa6a0-a07a-4a90-8c15-bf369e7d6627",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99fe58ea-f58e-44e2-8870-91b5118ef28a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e292039c-0162-4eb8-8908-1837e3d23896",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99fe58ea-f58e-44e2-8870-91b5118ef28a",
                    "LayerId": "624632d8-dbc0-4b71-8d05-f650128ece73"
                }
            ]
        },
        {
            "id": "864f2f71-c472-4ea1-9775-f8b3fcd79e6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b3b8b9-1da2-4efc-80f1-eb6a20d91ae9",
            "compositeImage": {
                "id": "565f08f6-8cc1-4dc1-80a4-0c67546b034f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "864f2f71-c472-4ea1-9775-f8b3fcd79e6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5defaec2-165f-418d-966a-ca434f681a0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "864f2f71-c472-4ea1-9775-f8b3fcd79e6c",
                    "LayerId": "624632d8-dbc0-4b71-8d05-f650128ece73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "624632d8-dbc0-4b71-8d05-f650128ece73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37b3b8b9-1da2-4efc-80f1-eb6a20d91ae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "27777447-a745-496f-8cba-7c522e2a58d4",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}