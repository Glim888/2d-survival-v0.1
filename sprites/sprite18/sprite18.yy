{
    "id": "acb3cb0e-f3cc-4141-883d-d02c30289303",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite18",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52aa69bf-8d26-49e5-9540-f9259cc9a4a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acb3cb0e-f3cc-4141-883d-d02c30289303",
            "compositeImage": {
                "id": "9747b150-8c2c-4729-ac40-54f5bbdf0a8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52aa69bf-8d26-49e5-9540-f9259cc9a4a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "941bc9a8-3f0a-4dbe-8991-ac771b070cf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52aa69bf-8d26-49e5-9540-f9259cc9a4a5",
                    "LayerId": "51394cbd-51cf-4905-ac0d-a253db4ac2d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "51394cbd-51cf-4905-ac0d-a253db4ac2d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acb3cb0e-f3cc-4141-883d-d02c30289303",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}