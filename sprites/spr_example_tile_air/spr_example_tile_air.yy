{
    "id": "7a5afd10-93ae-44f6-b4a6-4d69a6ed898d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_example_tile_air",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "efe0acf8-b1d6-45dc-a043-8e697c5cba06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a5afd10-93ae-44f6-b4a6-4d69a6ed898d",
            "compositeImage": {
                "id": "d7f808f9-160a-4a48-9a9b-4153125bbacc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efe0acf8-b1d6-45dc-a043-8e697c5cba06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2aa9c0b-9971-43cb-9c33-eaa9ee8d2b75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe0acf8-b1d6-45dc-a043-8e697c5cba06",
                    "LayerId": "33f54756-8775-4e10-af48-572bc4822047"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "33f54756-8775-4e10-af48-572bc4822047",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a5afd10-93ae-44f6-b4a6-4d69a6ed898d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "27777447-a745-496f-8cba-7c522e2a58d4",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}