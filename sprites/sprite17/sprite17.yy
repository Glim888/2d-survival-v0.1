{
    "id": "789db2fb-d396-4472-908c-ded2be0673e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite17",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cc89bdf-e18a-4da2-a254-7e9b7232a664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "789db2fb-d396-4472-908c-ded2be0673e5",
            "compositeImage": {
                "id": "72deb751-7e1d-463d-8147-b711ea516f4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cc89bdf-e18a-4da2-a254-7e9b7232a664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eee74659-e3f9-4040-8be2-f699d0d74360",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cc89bdf-e18a-4da2-a254-7e9b7232a664",
                    "LayerId": "80adb892-004f-4049-9c81-3060103a0543"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "80adb892-004f-4049-9c81-3060103a0543",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "789db2fb-d396-4472-908c-ded2be0673e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}