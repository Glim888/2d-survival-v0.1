{
    "id": "678c80a3-0c42-404c-83d4-8e724cedb367",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_tree_stump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2fdd799c-46ca-4c9f-9e79-e7ef276c51ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c80a3-0c42-404c-83d4-8e724cedb367",
            "compositeImage": {
                "id": "3a994527-5e49-4c31-a6ed-b13c3f1cae04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fdd799c-46ca-4c9f-9e79-e7ef276c51ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10f94137-2186-424a-8d69-094f448cd824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fdd799c-46ca-4c9f-9e79-e7ef276c51ab",
                    "LayerId": "913a0923-b274-4ca2-84d6-ac9db3ace785"
                }
            ]
        },
        {
            "id": "987e5777-04d8-47fb-9463-3a58545f7d90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678c80a3-0c42-404c-83d4-8e724cedb367",
            "compositeImage": {
                "id": "f26d14a4-c55f-40c8-afc7-50ee37cdfe98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "987e5777-04d8-47fb-9463-3a58545f7d90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8598bb27-34f1-4046-95b9-4998811c7c0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "987e5777-04d8-47fb-9463-3a58545f7d90",
                    "LayerId": "913a0923-b274-4ca2-84d6-ac9db3ace785"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "913a0923-b274-4ca2-84d6-ac9db3ace785",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "678c80a3-0c42-404c-83d4-8e724cedb367",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}