{
    "id": "d2aad3cf-f5c8-4e0a-8a72-445dcd5a045f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_coal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 4,
    "bbox_right": 38,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13ca4a1d-bf55-4f6c-843a-9b46c09ed4bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2aad3cf-f5c8-4e0a-8a72-445dcd5a045f",
            "compositeImage": {
                "id": "1bf2069b-0e11-410f-965a-370afd263e63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13ca4a1d-bf55-4f6c-843a-9b46c09ed4bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa4778f8-1210-4eb5-81ef-19cf5c7b294e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ca4a1d-bf55-4f6c-843a-9b46c09ed4bb",
                    "LayerId": "5dae3296-7b34-4fd2-9a86-39125d1f7a35"
                }
            ]
        },
        {
            "id": "ada98bc1-16af-4e47-9228-855fec9e0662",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2aad3cf-f5c8-4e0a-8a72-445dcd5a045f",
            "compositeImage": {
                "id": "ec9d62ce-9b4e-471c-9060-77f635e1cc27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ada98bc1-16af-4e47-9228-855fec9e0662",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ff2c07-4821-4672-9d81-3a6d44287d92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ada98bc1-16af-4e47-9228-855fec9e0662",
                    "LayerId": "5dae3296-7b34-4fd2-9a86-39125d1f7a35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "5dae3296-7b34-4fd2-9a86-39125d1f7a35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2aad3cf-f5c8-4e0a-8a72-445dcd5a045f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}