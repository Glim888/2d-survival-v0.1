{
    "id": "3e25070b-05ed-4244-900b-51d9a614b067",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_gL2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b402716-194c-4db6-8144-82d91852725d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e25070b-05ed-4244-900b-51d9a614b067",
            "compositeImage": {
                "id": "15b99a22-1ef2-4735-9674-43eb6f7070f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b402716-194c-4db6-8144-82d91852725d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "196ad86d-0deb-4116-9147-cb7772c3dded",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b402716-194c-4db6-8144-82d91852725d",
                    "LayerId": "b27fdaa1-dedf-406e-9815-15298940a3e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b27fdaa1-dedf-406e-9815-15298940a3e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e25070b-05ed-4244-900b-51d9a614b067",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "27777447-a745-496f-8cba-7c522e2a58d4",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}