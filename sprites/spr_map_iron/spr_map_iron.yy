{
    "id": "62923cdb-bba9-43e6-ab84-dc0aecc42505",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_iron",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c8cc33e-2ecb-43da-9335-46372adfdc36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62923cdb-bba9-43e6-ab84-dc0aecc42505",
            "compositeImage": {
                "id": "2a57a8a3-8b80-449f-901c-69b47157ed35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c8cc33e-2ecb-43da-9335-46372adfdc36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "994fe612-3410-4925-8feb-ca408ebbb58f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c8cc33e-2ecb-43da-9335-46372adfdc36",
                    "LayerId": "a4c25640-b6a9-4ad9-945b-dcea563e884d"
                }
            ]
        },
        {
            "id": "560fb56e-6d76-4e31-b7ea-d7095c6f9871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62923cdb-bba9-43e6-ab84-dc0aecc42505",
            "compositeImage": {
                "id": "5cbf2ffc-6b6c-4f1d-b4af-c3f6299e2dbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "560fb56e-6d76-4e31-b7ea-d7095c6f9871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c521dfff-0b7f-4305-b31e-a25aaff5f2c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "560fb56e-6d76-4e31-b7ea-d7095c6f9871",
                    "LayerId": "a4c25640-b6a9-4ad9-945b-dcea563e884d"
                }
            ]
        },
        {
            "id": "10f6339f-b499-4558-83e7-e03ea1bfdf63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62923cdb-bba9-43e6-ab84-dc0aecc42505",
            "compositeImage": {
                "id": "761f243f-f2f9-4301-9e3e-83f8c1e2ab9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10f6339f-b499-4558-83e7-e03ea1bfdf63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45fd93d0-50fd-47e4-aa66-6ebe50f52a0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10f6339f-b499-4558-83e7-e03ea1bfdf63",
                    "LayerId": "a4c25640-b6a9-4ad9-945b-dcea563e884d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "a4c25640-b6a9-4ad9-945b-dcea563e884d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62923cdb-bba9-43e6-ab84-dc0aecc42505",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}