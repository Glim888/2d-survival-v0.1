{
    "id": "7c424cb2-6d30-466e-8036-f8ddd7f1a1b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_snow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6585cdb7-baf8-4814-9980-4fa996688e71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c424cb2-6d30-466e-8036-f8ddd7f1a1b6",
            "compositeImage": {
                "id": "267dfb93-0465-42f4-b905-3d8cd4352c0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6585cdb7-baf8-4814-9980-4fa996688e71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "567c97af-7ac7-4483-95fa-35e03e2c5a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6585cdb7-baf8-4814-9980-4fa996688e71",
                    "LayerId": "c19769ba-2a96-4afd-848a-76e480c92e51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c19769ba-2a96-4afd-848a-76e480c92e51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c424cb2-6d30-466e-8036-f8ddd7f1a1b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "27777447-a745-496f-8cba-7c522e2a58d4",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}