{
    "id": "e3a80f7e-dfa9-4f23-bc80-710ef435d51c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_swamp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6ed463a-1f22-4571-8da4-795fdcb14dd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3a80f7e-dfa9-4f23-bc80-710ef435d51c",
            "compositeImage": {
                "id": "bf50b842-7be5-455b-8501-fb26a7ed1aa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6ed463a-1f22-4571-8da4-795fdcb14dd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e185cf07-f6fe-4a44-974f-fd6bd98566cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6ed463a-1f22-4571-8da4-795fdcb14dd1",
                    "LayerId": "39c97bef-0b9e-4a45-9b9a-75ad3feed8fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "39c97bef-0b9e-4a45-9b9a-75ad3feed8fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3a80f7e-dfa9-4f23-bc80-710ef435d51c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "27777447-a745-496f-8cba-7c522e2a58d4",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}