{
    "id": "f573c819-ba14-402c-976b-fc286c5dc199",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf81356d-32f4-4b8a-a490-6eb612d73394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f573c819-ba14-402c-976b-fc286c5dc199",
            "compositeImage": {
                "id": "30c99632-1b95-443b-be73-e91a36d7312d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf81356d-32f4-4b8a-a490-6eb612d73394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb6157f-67f5-4395-8b7a-f3b772aa3b92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf81356d-32f4-4b8a-a490-6eb612d73394",
                    "LayerId": "57c56286-f49c-49e9-b121-334c8773198c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "57c56286-f49c-49e9-b121-334c8773198c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f573c819-ba14-402c-976b-fc286c5dc199",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}