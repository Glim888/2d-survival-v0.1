{
    "id": "98a8115e-454f-44e2-9981-7dc54466448d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 16,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "591c6f20-fe07-4534-9e9f-875f174acaa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98a8115e-454f-44e2-9981-7dc54466448d",
            "compositeImage": {
                "id": "e01045c6-9759-435d-8399-f7f0c784e567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "591c6f20-fe07-4534-9e9f-875f174acaa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fc72d32-6f66-4ca0-b2fe-42a3af54bd9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "591c6f20-fe07-4534-9e9f-875f174acaa5",
                    "LayerId": "86639bec-ff9c-4504-ab20-234cea54cbaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "86639bec-ff9c-4504-ab20-234cea54cbaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98a8115e-454f-44e2-9981-7dc54466448d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}