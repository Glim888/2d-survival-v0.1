{
    "id": "f3d43334-8b13-4d51-98f3-b477175da48c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_tree_big",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25c335a3-ce48-46f1-baa3-8e36f41eba97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3d43334-8b13-4d51-98f3-b477175da48c",
            "compositeImage": {
                "id": "a92b2db4-c5cd-4361-8a80-e41efc7bc7b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25c335a3-ce48-46f1-baa3-8e36f41eba97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d312006-8fb7-44cb-8d11-e903d8a48f08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25c335a3-ce48-46f1-baa3-8e36f41eba97",
                    "LayerId": "07e0f873-f446-4e78-939f-6f0f8f8852a6"
                }
            ]
        },
        {
            "id": "fecdada6-4bcd-4496-acd0-f268c57d847a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3d43334-8b13-4d51-98f3-b477175da48c",
            "compositeImage": {
                "id": "414a12c5-5ec5-4680-9ce2-d03f51b1f84e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fecdada6-4bcd-4496-acd0-f268c57d847a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e1f308e-3f50-47aa-9ccf-85d8076b598a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fecdada6-4bcd-4496-acd0-f268c57d847a",
                    "LayerId": "07e0f873-f446-4e78-939f-6f0f8f8852a6"
                }
            ]
        },
        {
            "id": "a7770d9e-d9c4-4caf-8633-dc2e0f239a81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3d43334-8b13-4d51-98f3-b477175da48c",
            "compositeImage": {
                "id": "19213907-0f05-46e4-8cfc-4f2d9fb821b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7770d9e-d9c4-4caf-8633-dc2e0f239a81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0c7e8b8-283b-400c-be18-2d0d90286411",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7770d9e-d9c4-4caf-8633-dc2e0f239a81",
                    "LayerId": "07e0f873-f446-4e78-939f-6f0f8f8852a6"
                }
            ]
        },
        {
            "id": "883620bd-109c-425b-accd-e4957ede707a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3d43334-8b13-4d51-98f3-b477175da48c",
            "compositeImage": {
                "id": "46fa4be6-30bc-4116-b9ca-aed7fe1acafb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "883620bd-109c-425b-accd-e4957ede707a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2551c619-8315-4984-b875-e8773e41102e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "883620bd-109c-425b-accd-e4957ede707a",
                    "LayerId": "07e0f873-f446-4e78-939f-6f0f8f8852a6"
                }
            ]
        },
        {
            "id": "15bb1f8f-2aa9-4d5a-b413-aa3b2ed014ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3d43334-8b13-4d51-98f3-b477175da48c",
            "compositeImage": {
                "id": "27341426-cd87-4084-88c1-3fdaab690ccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15bb1f8f-2aa9-4d5a-b413-aa3b2ed014ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29428353-955e-407c-9569-e60b7239c1a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15bb1f8f-2aa9-4d5a-b413-aa3b2ed014ba",
                    "LayerId": "07e0f873-f446-4e78-939f-6f0f8f8852a6"
                }
            ]
        },
        {
            "id": "9c2754ce-984c-41ba-a34e-196f62ee8a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3d43334-8b13-4d51-98f3-b477175da48c",
            "compositeImage": {
                "id": "34a2a646-92cf-41eb-a19b-72f79b3df6f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c2754ce-984c-41ba-a34e-196f62ee8a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e23f334-df7a-489b-8ded-3b3f855140c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c2754ce-984c-41ba-a34e-196f62ee8a9b",
                    "LayerId": "07e0f873-f446-4e78-939f-6f0f8f8852a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 59,
    "layers": [
        {
            "id": "07e0f873-f446-4e78-939f-6f0f8f8852a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3d43334-8b13-4d51-98f3-b477175da48c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 49
}