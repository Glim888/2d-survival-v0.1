{
    "id": "3570f4b4-cc65-49aa-b505-0eebb3691abf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_gL3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e910451-af95-400e-b272-40b7fd70bfd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3570f4b4-cc65-49aa-b505-0eebb3691abf",
            "compositeImage": {
                "id": "d3a13e88-e40b-489c-99fb-5d4c2171d1c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e910451-af95-400e-b272-40b7fd70bfd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "097c2325-0031-4a7c-a3d2-b65ccee8be45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e910451-af95-400e-b272-40b7fd70bfd2",
                    "LayerId": "c1a28855-e713-4e8e-94ce-94fc9b479c68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c1a28855-e713-4e8e-94ce-94fc9b479c68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3570f4b4-cc65-49aa-b505-0eebb3691abf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "27777447-a745-496f-8cba-7c522e2a58d4",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}