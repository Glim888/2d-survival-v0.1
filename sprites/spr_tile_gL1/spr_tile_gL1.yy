{
    "id": "bb5b3b37-9e56-4c1a-84da-0ec62d057798",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_gL1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b4a03d1-88ed-4345-8e03-f6850621b814",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb5b3b37-9e56-4c1a-84da-0ec62d057798",
            "compositeImage": {
                "id": "43618ef3-701e-4fbc-93c2-2f9eec96272d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b4a03d1-88ed-4345-8e03-f6850621b814",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc3ed5a3-b25e-497d-887f-4858a2b5a872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b4a03d1-88ed-4345-8e03-f6850621b814",
                    "LayerId": "0e05e100-2dc5-4a5b-b92e-1983ed869034"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0e05e100-2dc5-4a5b-b92e-1983ed869034",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb5b3b37-9e56-4c1a-84da-0ec62d057798",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "27777447-a745-496f-8cba-7c522e2a58d4",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}