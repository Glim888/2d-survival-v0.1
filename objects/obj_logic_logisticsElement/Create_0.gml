/// @description

/*
	data container for obj_logic_logistics
*/

isAssemblyLine = ERROR;		// is a building, or a assemblyLine
instance = ERROR;			// the linked sMO 
itemID = ERROR;				// the item, which the sMO instance outputs
usedCells = ERROR;			// the cells, on which the sMO stands [in Tiles as ds_list]
chunkID = ERROR;			// the chunk, on which the sMO stands