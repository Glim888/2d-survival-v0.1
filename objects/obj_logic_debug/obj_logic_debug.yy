{
    "id": "89314a6e-c758-4a27-9353-9ff5621117c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_logic_debug",
    "eventList": [
        {
            "id": "770d2cbf-4060-4156-8f33-e7031a90e0b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "89314a6e-c758-4a27-9353-9ff5621117c3"
        },
        {
            "id": "818e8682-e27d-4d4c-a62a-2678b113655f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89314a6e-c758-4a27-9353-9ff5621117c3"
        },
        {
            "id": "d6a12bc8-5eed-4101-992b-71036ebf5c3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "89314a6e-c758-4a27-9353-9ff5621117c3"
        },
        {
            "id": "43aeae8e-8e69-4e6a-9ca2-a553bc55b01b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "89314a6e-c758-4a27-9353-9ff5621117c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}