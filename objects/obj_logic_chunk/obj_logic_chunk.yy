{
    "id": "8c342259-86eb-4df2-ac9d-2f78bfb90d57",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_logic_chunk",
    "eventList": [
        {
            "id": "a324a482-9bb4-481d-980c-2937dc6adb47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8c342259-86eb-4df2-ac9d-2f78bfb90d57"
        },
        {
            "id": "dd148a0a-6ade-41d7-b691-7196dd8fd66a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8c342259-86eb-4df2-ac9d-2f78bfb90d57"
        },
        {
            "id": "65381c51-35a7-40d0-9a4f-56c68261861d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8c342259-86eb-4df2-ac9d-2f78bfb90d57"
        },
        {
            "id": "ceeb23c8-4225-4410-9531-39cf14b4bde7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8c342259-86eb-4df2-ac9d-2f78bfb90d57"
        },
        {
            "id": "fe3a0047-1d27-4ba8-8002-b44bf4a219a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "8c342259-86eb-4df2-ac9d-2f78bfb90d57"
        },
        {
            "id": "02f9e8aa-1f67-4fd1-aa79-0b66e8d48a5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8c342259-86eb-4df2-ac9d-2f78bfb90d57"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}