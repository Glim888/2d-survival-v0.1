{
    "id": "8ebafa36-f19a-430b-b919-24227197a09e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sMO",
    "eventList": [
        {
            "id": "80aa4d05-cca4-4a72-b9dd-0d2375794d8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8ebafa36-f19a-430b-b919-24227197a09e"
        },
        {
            "id": "d2f73a1a-fa0a-466b-88ac-04333aad8034",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "8ebafa36-f19a-430b-b919-24227197a09e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bfd9b54a-cfd0-4226-abbc-e5c26915c672",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}