{
    "id": "aa9445b8-6dec-49bb-8b8d-5ca2a5425f76",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mO_res_treeStump",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6fd06c0b-0c1c-4534-bf6e-9e1a519ca5ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "678c80a3-0c42-404c-83d4-8e724cedb367",
    "visible": true
}