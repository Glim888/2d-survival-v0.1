{
    "id": "4d142fee-4217-466e-8528-d47b4c366001",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_logic_inventory",
    "eventList": [
        {
            "id": "4a9bd6b3-cebc-4faa-8dc9-e1725f2d162b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "4d142fee-4217-466e-8528-d47b4c366001"
        },
        {
            "id": "fcb22e43-a109-4cb0-996f-b8beb10b6e54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "4d142fee-4217-466e-8528-d47b4c366001"
        },
        {
            "id": "b60f5bbf-0066-49f8-b51e-c4dc120c388c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "4d142fee-4217-466e-8528-d47b4c366001"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}