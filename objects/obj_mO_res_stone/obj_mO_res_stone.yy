{
    "id": "5334d623-1cda-4fcd-9f65-95db7f6f6af5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mO_res_stone",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6fd06c0b-0c1c-4534-bf6e-9e1a519ca5ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "28e6d080-dc11-43d4-9325-d5e02ccf81f8",
    "visible": true
}