{
    "id": "343dec39-e696-43c1-8b5e-818cfb7bf46f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_logic_handler",
    "eventList": [
        {
            "id": "02713823-af6e-4b02-b2d4-7bbc4284c271",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "343dec39-e696-43c1-8b5e-818cfb7bf46f"
        },
        {
            "id": "1b0d918c-d3b7-4830-84e8-e4227cca11fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "343dec39-e696-43c1-8b5e-818cfb7bf46f"
        },
        {
            "id": "f5fc1c3c-7072-455c-a25f-62881a5bd5d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "343dec39-e696-43c1-8b5e-818cfb7bf46f"
        },
        {
            "id": "259c4e64-cc1f-46c9-bc8d-bb7148a37c52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "343dec39-e696-43c1-8b5e-818cfb7bf46f"
        },
        {
            "id": "7e808289-4d5a-485c-9afd-b2141321262d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "343dec39-e696-43c1-8b5e-818cfb7bf46f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}