{
    "id": "79580e67-3ae2-4975-8785-695ee0a3c5ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mO_bld_assemblyLine",
    "eventList": [
        {
            "id": "4254306a-1906-4833-9ee5-cdbf3abe2538",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "79580e67-3ae2-4975-8785-695ee0a3c5ba"
        },
        {
            "id": "33173885-6051-4bde-905a-f1ec0cabbf8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "79580e67-3ae2-4975-8785-695ee0a3c5ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "18a07e06-1d73-4d47-8d60-392a4c6fb5e7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "789db2fb-d396-4472-908c-ded2be0673e5",
    "visible": true
}