{
    "id": "fe108b42-e241-43d9-b9bd-a3b51d553739",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mO_bld_prd_furnanceStone",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9325df95-6f1b-4cee-b668-03de55a37ea7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0c79eb1a-1863-4917-9a65-8a206874f5b3",
    "visible": true
}