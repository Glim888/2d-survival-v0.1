{
    "id": "fae8dec4-d30b-4642-b359-317b72249bd5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mO_bld_prd_mineIron1",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9325df95-6f1b-4cee-b668-03de55a37ea7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "acb3cb0e-f3cc-4141-883d-d02c30289303",
    "visible": true
}