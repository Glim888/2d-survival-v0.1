{
    "id": "34be0b2d-8531-4833-9cdc-129cdd7cdbf5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_logic_item",
    "eventList": [
        {
            "id": "a52f8a26-59f7-4a03-b713-9707e07c007f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34be0b2d-8531-4833-9cdc-129cdd7cdbf5"
        },
        {
            "id": "e3017c0e-f462-4b02-84ca-f45e0c4e4fa5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "34be0b2d-8531-4833-9cdc-129cdd7cdbf5"
        },
        {
            "id": "2cfa85b4-333b-4ee8-8158-fcb68c7fb54d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "34be0b2d-8531-4833-9cdc-129cdd7cdbf5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}