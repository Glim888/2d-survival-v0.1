{
    "id": "9325df95-6f1b-4cee-b668-03de55a37ea7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mO_bld_prd",
    "eventList": [
        {
            "id": "24515a72-ff3c-44f6-92e1-fe1c9218ea7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "9325df95-6f1b-4cee-b668-03de55a37ea7"
        },
        {
            "id": "b9f2dd04-97c9-429c-8f98-af8014d8f15a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9325df95-6f1b-4cee-b668-03de55a37ea7"
        },
        {
            "id": "7026ce75-0d44-4d0b-b0aa-d4022e4c22f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9325df95-6f1b-4cee-b668-03de55a37ea7"
        },
        {
            "id": "c1532b0a-81c8-4909-b1bb-856d29adc268",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "9325df95-6f1b-4cee-b668-03de55a37ea7"
        },
        {
            "id": "a161c989-4990-43dd-9e47-da342c7cd605",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9325df95-6f1b-4cee-b668-03de55a37ea7"
        },
        {
            "id": "2f4a5ab9-7e45-41a7-a705-6b788b9b21ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9325df95-6f1b-4cee-b668-03de55a37ea7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "18a07e06-1d73-4d47-8d60-392a4c6fb5e7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}