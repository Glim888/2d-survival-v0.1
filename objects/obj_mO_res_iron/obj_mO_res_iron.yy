{
    "id": "8aa7acf9-1fd2-4678-917c-f836f01c12a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mO_res_iron",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6fd06c0b-0c1c-4534-bf6e-9e1a519ca5ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "62923cdb-bba9-43e6-ab84-dc0aecc42505",
    "visible": true
}