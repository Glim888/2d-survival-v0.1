{
    "id": "ab357adc-9f93-47ff-af39-c629676d009f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mO_res_treeBig",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6fd06c0b-0c1c-4534-bf6e-9e1a519ca5ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f3d43334-8b13-4d51-98f3-b477175da48c",
    "visible": true
}