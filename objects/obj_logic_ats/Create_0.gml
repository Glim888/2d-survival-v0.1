/// @description

depth = TILE_DEPTH;

var _priority = ds_priority_create();


ds_priority_add(_priority, spr_example_tile_air, TILE_DEPTH+6);
ds_priority_add(_priority, spr_tile_dessert, TILE_DEPTH+5);
ds_priority_add(_priority, spr_tile_swamp, TILE_DEPTH+4);
ds_priority_add(_priority, spr_tile_gL1, TILE_DEPTH+3);
ds_priority_add(_priority, spr_tile_gL2, TILE_DEPTH+2);
ds_priority_add(_priority, spr_tile_gL3, TILE_DEPTH+1);
ds_priority_add(_priority, spr_tile_snow, TILE_DEPTH+0);


ats_init(TILE_SIZE, TILE_SIZE, _priority, 4);
ds_priority_destroy(_priority);


