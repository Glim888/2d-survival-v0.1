{
    "id": "e529fe63-af48-40a0-ab04-8871fba9e9d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_logic_ats",
    "eventList": [
        {
            "id": "f2411adc-d6a4-474d-b33c-a19a0d169cec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e529fe63-af48-40a0-ab04-8871fba9e9d6"
        },
        {
            "id": "5453237d-341a-49a4-ab61-c5ec484ca117",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e529fe63-af48-40a0-ab04-8871fba9e9d6"
        },
        {
            "id": "e7b2d0fc-c19b-4f9b-831d-2ebc25e8683e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e529fe63-af48-40a0-ab04-8871fba9e9d6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}