{
    "id": "7aa0e66b-c346-439e-a7eb-b1ae76a987b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mO_res_coal",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6fd06c0b-0c1c-4534-bf6e-9e1a519ca5ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d2aad3cf-f5c8-4e0a-8a72-445dcd5a045f",
    "visible": true
}