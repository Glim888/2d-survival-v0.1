/// @func logistics_deregister
/// @param logisticsElement
/// @param updateGrid?
/// @desc deregister a Building or a AssemblyLine
/// @return 

with (obj_logic_logistics) {
	
	if (ds_list_find_index(buildings, argument0) != -1 ||
		ds_list_find_index(assemblyLines, argument0) != -1) {
		

		if (argument0.isAssemblyLine) {			
			ds_list_delete(assemblyLines, ds_list_find_index(assemblyLines, argument0));	
		}else{			
			ds_list_delete(buildings, ds_list_find_index(buildings, argument0));			
		}
		
			
		ds_list_destroy(argument0.usedCells);
		ds_list_destroy(argument0.connectedLogisticsElements);
		path_delete(argument0.path);
		instance_activate_object(argument0);
		instance_destroy(argument0);
		if (argument1) __logistics_updateGridSize();
		
	}
	
}