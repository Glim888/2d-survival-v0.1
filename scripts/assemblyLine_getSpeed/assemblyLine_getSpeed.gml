/// @func assemblyLine_getSpeed
/// @param assemblyLine Id
/// @desc get current speed of assemblyLine
/// @return speed of assemblyLine

with (argument0) return aL_speed;