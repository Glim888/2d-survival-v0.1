/// @func data_imageIndex
/// @param object_index
/// @desc get image_index, based on object_index
/// @return image_index

switch (argument0) {
	
		case obj_mO_res_treeBig: return sMO_biome; break;
		case obj_mO_res_treeSmall: return  sMO_biome*2 + irandom(1); break;
		default: return 0; break;
		
	}