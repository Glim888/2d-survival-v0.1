/// @func __res_getHarvestHp
/// @param
/// @desc returns the arvest_hp based on object_index
/// @return harvest hp


switch (object_index) {
	
	case obj_mO_res_treeSmall: return res_hp.tree_small; break;
	case obj_mO_res_treeBig: return res_hp.tree_big;; break;
	case obj_mO_res_coal: return res_hp.coal; break;
	case obj_mO_res_iron: return res_hp.iron; break;
	case obj_mO_res_stone: return res_hp.stone; break;
	case obj_mO_res_treeStump: return res_hp.tree_stump; break;
	default: show_error("get_harvestHp -> Switch index", 1); break;
		
}
