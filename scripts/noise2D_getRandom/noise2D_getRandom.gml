/// @func noise2D_getRandom
/// @param cellX
/// @param cellY
/// @param seed
/// @desc calculate noise
/// @return noiseValue (-1 -> 1)
gml_pragma("forceinline");

var _val = argument0 + argument1 * 57 + argument2;

_val = (_val << 13) ^ _val;

return  1 - ( (_val * (_val * _val * 15731 + 789221) + 1376312589) & $7fffffff) * 0.000000000931322574615478515625;
