/// @func data_itemID
/// @param object_index
/// @desc get itemID based on object_index
/// @return sMO_itemID

switch (argument0) {
	
	case obj_mO_res_treeSmall: return item_Id.wood; break;
	case obj_mO_res_treeBig: return item_Id.wood; break;
	case obj_mO_res_coal: return item_Id.coal; break;
	case obj_mO_res_iron: return item_Id.iron; break;
	case obj_mO_res_stone: return item_Id.stone; break;
	case obj_mO_res_treeStump: return item_Id.wood; break;
	default: Log("data_itemID-Default!!!"); return item_Id.wood; break;
		
}
