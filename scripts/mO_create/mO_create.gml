/// @func mO_create
/// @param x
/// @param y
/// @param object_index
/// @param chunk_id
/// @param buffer_offset
/// @param biome
/// @desc create a Map Object + some creation logic
/// @return mapObject instance id

var _inst = instance_create_layer( argument0, argument1, "Instances", argument2);

handlerMO_registerMapObject(_inst);

with (_inst) {
	
	chunk_id = argument3;
	buffer_offset = argument4;	
	
	// image_index based on biome
	switch (object_index) {
	
		case obj_mO_res_treeBig: image_index = argument5; break;
		case obj_mO_res_treeSmall: image_index = argument5*2 + irandom(1); break; // TODO
		default: break;
		
	}
	
	if (!point_in_rectangle(x, y, obj_player.x - VIEW_W*0.6, obj_player.y - VIEW_H*0.6,
								 obj_player.x + VIEW_W*0.6, obj_player.y + VIEW_H*0.6)) {
		mO_deactivate(self);			 
	}
}

return _inst;

				