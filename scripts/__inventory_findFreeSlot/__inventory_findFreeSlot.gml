/// @func __inventory_findFreeSlot
/// @param
/// @desc search a free slot in inventory
/// @return position in inventory grid[as array] or ERROR

var _pos = [];

for (var _y=0; _y<inv_h; _y++) {
		for (var _x=0; _x<inv_w; _x++) {
			if (inv_grid[# _x, _y] == noone) {
				_pos[0] = _x;
				_pos[1] = _y;
				return _pos;
			}
		}
}

_pos[0] = ERROR;
return _pos;




