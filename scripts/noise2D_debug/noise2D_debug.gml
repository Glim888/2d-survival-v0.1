/// @func noise2D_debug
/// @param noise2D_grid
/// @param size of debug rectangles
/// @desc debug noise
/// @return

if (event_type != ev_draw) return;
 
for (var _y=0; _y<ds_grid_height(argument0); _y++) {
	for (var _x=0; _x<ds_grid_width(argument0); _x++) {
		var _c = (argument0[# _x, _y]) +1;	
		var col = make_color_rgb(_c, _c, _c);
		draw_rectangle_color(_x*argument1, _y*argument1, (_x+1)*argument1, (_y+1)*argument1, col, col, col, col, 0);
	}
}