/// @func inventory_addItemAtPosition
/// @param inventoryId
/// @param itemID
/// @param xCell
/// @param yCell
/// @desc add a item to inventory at position x/y
/// @return successfull or not

with (argument0) {
	
	// Create a new slot, if at the desired position is no slot
	if (inv_grid[# argument2 , argument3] == noone) {
		__inventory_createSlot(argument2, argument3, argument1, 0);
	}
	
	if (inv_grid[# argument2 , argument3].itemID == argument1) {			
		if (inv_grid[# argument2 , argument3].itemNumber + 1 <= INV_MAX) {
			inv_grid[# argument2 , argument3].itemNumber++;
			return true;
		}
	}
}
return false;

