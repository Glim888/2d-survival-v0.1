/// @func chunk_checkOutOfScope
/// @param centerChunkX
/// @param centerChunkY
/// @desc takes the current center of all chunks and destroys all chunks which are to far away (out of scope)
/// @return 



with(obj_logic_chunk) {
	if (abs(chunk_xCell - argument0) > CHUNK_RAD || abs(chunk_yCell - argument1) > CHUNK_RAD) {
		instance_destroy();
	}
}