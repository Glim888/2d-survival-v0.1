/// @func prd_sendItem
/// @param prd Instance
/// @param path
/// @desc prd will try to send a item along the path
/// @return success or not


with (argument0) {
	
	if (inventory_getItemNumber(prd_items, prd_outputItem) > 0) {
		
		inventory_removeItem(prd_items, prd_outputItem, 1);
		item_create(0, 0, prd_outputItem, argument1);
		return true;
	}		
}

return false;
	
	