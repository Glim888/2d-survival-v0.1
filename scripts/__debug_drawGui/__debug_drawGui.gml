/// @func __debug_drawGui
/// @param 
/// @desc  is debuging the game by drawing values etc into the GUI
/// @return 

if (global.debug) {
	
	draw_set_color(c_white);

	draw_text(10, 50, "Numb (obj_logic_chunk): " +string(instance_number(obj_logic_chunk)));
	draw_text(10, 80, "Numb (inst): " +string(instance_count));
	if (benchmark) draw_text(10, 110, "Benchmark (real): " +string(benchmark_fps_real/benchmark_i));
	if (benchmark) draw_text(10, 130, "Benchmark (fps): " +string(benchmark_fps/benchmark_i));
	
	if (!benchmark) {
	
		inventory_debug(global.inventory, 100, 100, 50);
	
	}
	
	draw_set_color(c_black);
	
}
