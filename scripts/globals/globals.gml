/// @func globals
/// @param 
/// @desc declaring all global variables
/// @return 


global.debug = false;
global.seed = 126789;
global.noiseType = 3;
global.useInterpolation = true;
global.inventory = inventory_create("Inv", INVENTORY_SIZE, INVENTORY_SIZE);

