/// @func data_outputItem
/// @param object_index
/// @desc get outputItem, based on object_index
/// @return prd_outputItem

switch (argument0) {
	
		case obj_mO_bld_prd_mineIron: return item_Id.iron;
		case obj_mO_bld_prd_furnance: return item_Id.stone;
		
		default: Log("data_outputItem - Switch->Default");break;
		
	}