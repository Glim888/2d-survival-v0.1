/// @func chunk_drawTiles
/// @param 
/// @desc 
/// @return 

ats_rectangle_draw(atsRect, grid_tiles, 1, 1, ds_grid_width(grid_tiles)-2, ds_grid_height(grid_tiles)-2, chunk_xCell*CHUNK_SIZE*TILE_SIZE, chunk_yCell*CHUNK_SIZE*TILE_SIZE);