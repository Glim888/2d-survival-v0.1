/// @func res_harvestExecute
/// @param resource instance
/// @desc harvest logic
/// @return 

with (argument0) {
	
	harvest_flag = true;
	if (harvest_hp-- <=0) {
		chunk_addMapObject(chunk_id, buffer_offset, noone);
		mO_destroy(self);
		inventory_addItem(harvest_itemId, 3);
	}
}