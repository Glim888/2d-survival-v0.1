/// @func __prd_findItemPosition
/// @param item_Id
/// @desc find the defined item position in the inventory
/// @return position or ERROR

for (var _i=0; _i< prd_inputItemLen; _i++) if (prd_inputItem[_i] == argument0) return _i;

if (prd_outputItem == argument0) return prd_inputItemLen;

return ERROR;