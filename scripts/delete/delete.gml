/// @func delete
/// @param instance to delete
/// @desc free memory by deleting instance
/// @return 

instance_activate_object(argument0);
with(argument0) instance_destroy();