/// @func sMO_drawHP
/// @param sMO Instance
/// @desc draw healthbar of arg0
/// @return 

with (argument0) draw_healthbar(x- 20, y -5, x+20, y+5, map(sMO_hp, 0, data_Hp(object_index), 0, 100), c_black, c_red, c_green, 0, 0, 1);	
