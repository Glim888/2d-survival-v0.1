/// @func __logistics_updateGridSize
/// @param 
/// @desc updates the current gridSize
/// @return 

// TODO eigene Funktion??

var _len = ds_list_size(elements);
var _minX = 100000, _maxX = -100000, _minY = 100000, _maxY = -100000;
var _coords;

for (var _i=0; _i< _len; _i++) {
	
	_coords = chunk_getCellCoords(elements[| _i].chunkID);
	
	if (_coords != ERROR) {
		
		if (_coords[0] < _minX) _minX = _coords[0];
		if (_coords[0] > _maxX) _maxX = _coords[0];
		if (_coords[1] < _minY) _minY = _coords[1];
		if (_coords[1] > _maxY) _maxY = _coords[1];
		
	}
}

var _diffX = _maxX - _minX;
var _diffY = _maxY - _minY;

mp_grid_destroy(grid);
__logistic_createGrid(_minX*CHUNK_SIZE, _minY*CHUNK_SIZE, _maxX*CHUNK_SIZE, _maxY*CHUNK_SIZE);

