/// @func inventory_addItem
/// @param inventoryId
/// @param itemID
/// @param number of elements to add
/// @desc add n number of that item to inventory
/// @return success or not

with (argument0) {
	
	var _slots = __inventory_findItem(argument1);
	var _temp = argument2;
	
	for (var _i=0; _i<array_length_1d(_slots); _i+=2) {
				
		var _val = min(INV_MAX - inv_grid[# _slots[_i] , _slots[_i+1]].itemNumber, _temp);	
		inv_grid[# _slots[_i] , _slots[_i+1]].itemNumber += _val;
		_temp -= _val;
		if (_temp <= 0) return true;

	}
	var _breaker = 0;
	while(_temp > INV_MAX) {
	
		var _pos = __inventory_findFreeSlot();
		
		if (_pos[0] != ERROR) {
			var _val = min(INV_MAX, _temp);
			__inventory_createSlot(_pos[0], _pos[1], argument1, _val);
			_temp -= _val;
			if (_temp <= 0) return true;	
		}	
		if (_breaker++ > 100) break;
	}
}

return false;
