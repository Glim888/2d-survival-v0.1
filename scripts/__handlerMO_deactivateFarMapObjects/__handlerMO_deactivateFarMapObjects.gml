/// @func __handlerMO_deactivateFarMapObjects 
/// @param 
/// @desc deactivate all mapObjects, that are out of view --- activate all mapObjects that are in view
/// @return 

if (event_type != ev_alarm) show_error("wrong event", 0);

alarm[INST_DEAC_ALARM] = INST_DEAC_CYCLE;


instance_deactivate_object(obj_mO);
instance_activate_region(obj_player.x - VIEW_W*INST_DEAC_RAD,
						 obj_player.y - VIEW_H*INST_DEAC_RAD,
						 2*VIEW_W*INST_DEAC_RAD,
						 2*VIEW_H*INST_DEAC_RAD, 
						 1);
