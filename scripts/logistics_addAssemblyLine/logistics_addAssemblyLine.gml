/// @func logistics_addAssemblyLine
/// @param usedCells as List
/// @param chunkID
/// @desc 
/// @return 

with (obj_logic_logistics) {
	
	_inst = instance_create_layer(0, 0, "Logic", obj_logic_logisticsElement);

	with (_inst) {
	
		isAssemblyLine = true;
		usedCells = argument0; // [in Tiles as List]
		chunkID = argument1;

	}	
	ds_list_add(assemblyLines, _inst);
	if (!__logistics_chunkExists(argument1)) {
		ds_list_add(usedChunks, argument1);
		__logistics_updateGridSize();
	}
	
	__logistics_fillGrid(_inst);
}