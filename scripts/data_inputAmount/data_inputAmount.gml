/// @func data_inputAmount
/// @param object_index
/// @desc get inputAmount, based on object_index
/// @return prd_inputAmount


inputAmount[0] = ERROR;

switch (argument0) {
	
	case obj_mO_bld_prd_mineIron: inputAmount[0] = 0; break;
	case obj_mO_bld_prd_furnance: inputAmount[0] = 100; break;
	case obj_mO_bld_prd_mineCoal: inputAmount[0] = 0; break;
	case obj_mO_bld_prd_furnanceCoal: inputAmount[0] = 200; break;
	case obj_mO_bld_prd_furnanceStone: inputAmount[0] = 500; break;
	case obj_mO_bld_prd_mineStone: inputAmount[0] = 0; break;
		
	default: show_error("data_inputAmount - Switch->Default", true); break;	
}


return inputAmount;