/// @func chunkHandler_deregisterChunk
/// @param chunk
/// @desc deregister a chunk in "active_chunks" map
/// @return 

with (obj_logic_chunkHandler) {

	if (instance_exists(argument0)) {	
		ds_map_delete(obj_logic_chunkHandler.active_chunks, string(argument0.chunk_xCell)+"_"+string(argument0.chunk_yCell));	
	}else{
		show_error("chunkHandler_deregisterChunk chunk does not exist", 0);
	}
	
}