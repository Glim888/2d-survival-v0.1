/// @func item_getItemId
/// @param item instance
/// @desc get the itemID of the item instance
/// @return item_Id or ERROR

with (argument0) return itemID;

return ERROR;