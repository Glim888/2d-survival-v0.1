/// @func __prd_getUsedCells
/// @param 
/// @desc get used cells 
/// @return list of used Cells (x1,y1,x2,y2....)


var _list = ds_list_create();

var _xx = x div TILE_SIZE;
var _yy = y div TILE_SIZE;

_list[| 0] = _xx;
_list[| 1] = _yy;
_list[| 2] = _xx+1;
_list[| 3] = _yy;
_list[| 4] = _xx;
_list[| 5] = _yy+1;
_list[| 6] = _xx+1;
_list[| 7] = _yy+1;

return _list;

