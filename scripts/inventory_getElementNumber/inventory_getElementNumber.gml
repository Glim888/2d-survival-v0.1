/// @func inventory_getItemNumber
/// @param item
/// @desc returns the current element number of the specified item
/// @return number of elements or 0

with (obj_logic_inventory) {
	
	var _invPosition = __inventory_findItem(argument0);

	// item exists in inventory
	if (_invPosition[0] >= 0 ) {
		return inv_grid[# _invPosition[0] , _invPosition[1]].itemNumber;
	}
}

return 0;