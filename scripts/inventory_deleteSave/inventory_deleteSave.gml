/// @func inventory_deleteSave
/// @param inventoryId
/// @desc remove a save from disk
/// @return 

with (argument0) {
	
	if (file_exists(INV_SAV_DIR + inv_filename)) {
		file_delete(INV_SAV_DIR + inv_filename);
	}
}