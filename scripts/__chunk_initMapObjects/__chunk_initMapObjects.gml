/// @func __chunk_initMapObjects
/// @param 
/// @desc create mapObjects from previous generated Buffer -> Ressources; Buildings...
/// @return 
	
var _inst = noone, _mO_type, _id, _bio, _n=0;
buffer_offset = -1;
buffer_seek(buffer_mapObjects, buffer_seek_start, 0);
	
for (var _y=0; _y<CHUNK_SIZE; _y++) {
	for (var _x=0; _x<CHUNK_SIZE; _x++) {
			
			_mO_type  = buffer_read(buffer_mapObjects, MAPOBJECTS_T);
			buffer_offset++;
			if (_mO_type >= 0) {

				// @TODO Switch  -> ARRAY
				#region translate mapObj_Id (general) to real objects 
				switch (_mO_type) {
					
					case mapObj_Id.tree_stump: _id = obj_mO_res_treeStump; break;
					case mapObj_Id.tree_small: _id = obj_mO_res_treeSmall; break;
					case mapObj_Id.tree_big: _id = obj_mO_res_treeBig; break;
					case mapObj_Id.coal: _id = obj_mO_res_coal; break;
					case mapObj_Id.iron: _id = obj_mO_res_iron; break;
					case mapObj_Id.stone: _id = obj_mO_res_stone; break;
					case mapObj_Id.mineCoal: _id = obj_mO_bld_prd_mineCoal; break; // TEST
					default: continue; break;
					
				}
				#endregion
	
				_bio = buffer_peek(buffer_bioms, _x + _y*CHUNK_SIZE, BIOMS_T);
	
				_inst = sMO_create(	chunk_xCell*TILE_SIZE*CHUNK_SIZE + _x*TILE_SIZE +TILE_SIZE*0.5+ random_range(-MAP_OBJECTS_RND, MAP_OBJECTS_RND),
									chunk_yCell*TILE_SIZE*CHUNK_SIZE + _y*TILE_SIZE +TILE_SIZE*0.5+ random_range(-MAP_OBJECTS_RND, MAP_OBJECTS_RND),
									_id, self, buffer_offset, _bio);
				
				tilemap_set(tilemap_collision, 1, _x, _y);
				ds_list_add(mapObjects_list, _inst);
						
		}
	}
}	
