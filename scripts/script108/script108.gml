/// @func data_inputItem
/// @param object_index
/// @desc get inputItem, based on object_index
/// @return prd_inputItem as Array

array = ERROR;

switch (argument0) {
	
		case obj_mO_bld_prd_mineIron: array[0] = noone; break;
		case obj_mO_bld_prd_furnance: array[0] = item_Id.iron; break;
		
		default: Log("data_inputItem - Switch->Default");break;
		
	}


return array;