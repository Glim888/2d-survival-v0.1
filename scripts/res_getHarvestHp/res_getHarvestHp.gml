/// @func res_getHarvestHp
/// @param resource instance
/// @desc checks the object index and returns the defined value
/// @return harvest hp

with (argument0) {

	switch (object_index) {
	
		case obj_mO_res_treeSmall: return res_hp.tree_small; break;
		case obj_mO_res_treeBig: return res_hp.tree_big;; break;
		case obj_mO_res_coal: return res_hp.coal; break;
		case obj_mO_res_iron: return res_hp.iron; break;
		case obj_mO_res_stone: return res_hp.stone; break;
		case obj_mO_res_treeStump: return res_hp.tree_stump; break;
		default: show_error("get_harvestHp -> Switch index", 1); break;
		
	}

}