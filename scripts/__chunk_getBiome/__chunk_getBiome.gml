/// @func __chunk_getBiome
/// @param temp_val
/// @param moist_val
/// @desc check which biom is in this tile active based on temperature and moisture
/// @return biome_type


var _tv = argument0 + argument1 * random(0.01); 
var _mv = argument1 + argument0 * random(0.1); 

if (_tv > 210) {

	if (_mv < 128) return bioms.desert;
	return bioms.swamp;
	
}

if (_tv > 85) {
	
	if (_mv < 85) return bioms.grassland1
	if (_mv < 170) return bioms.grassland2
	return bioms.grassland3
	
}

if (_mv < 100) return bioms.snow
if (_mv < 170) return bioms.grassland2

return bioms.grassland3


