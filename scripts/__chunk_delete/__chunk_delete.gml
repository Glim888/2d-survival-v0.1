/// @func __chunk_delete
/// @desc delete/ free the Ram 
/// @return

__chunk_save();
	
if (layer_tilemap_exists(COLLISION_LAYER, tilemap_collision)) layer_tilemap_destroy(tilemap_collision);

var _size = ds_list_size(mapObjects_list);

for (var _i=0; _i<_size; _i++) instance_destroy(mapObjects_list[| _i]); 			
			
buffer_delete(buffer_bioms);
buffer_delete(buffer_mapObjects);
buffer_delete(buffer_entities);
	
ds_list_destroy(mapObjects_list);
ds_grid_destroy(grid_tiles);
instance_destroy(atsRect);

handlerCH_deregisterChunk(self);

instance_destroy();

