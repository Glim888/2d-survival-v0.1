/// @func handlerMO_registerToDrawHp
/// @param sMO Instance
/// @desc remove a sMO Instance from "drawHP_list", the handler will stop drawing the hp of that instance then
/// @return 

with (obj_logic_handler) ds_list_delete(drawHP_list, ds_list_find_index(drawHP_list, argument0));

