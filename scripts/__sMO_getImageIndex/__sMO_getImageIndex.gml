/// @func __sMO_getImageIndex
/// @param 
/// @desc set the image_index, based on object_index
/// @return 

switch (object_index) {
	
		case obj_mO_res_treeBig: return sMO_biome; break;
		case obj_mO_res_treeSmall: return  sMO_biome*2 + irandom(1); break; // CONST
		default: return 0; break;
		
	}