/// @func res_addDamage
/// @param resource
/// @param damage
/// @desc apply damage to the resource
/// @return 

with (argument0) {
		
	sMO_hp -= argument1;
	handlerMO_registerToDrawHp(self);
	__sMO_checkDead();
	return;

}

Log("res_addDamage -> wrong argument0");