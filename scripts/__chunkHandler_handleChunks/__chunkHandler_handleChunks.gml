/// @func __chunkHandler_handleChunks
/// @param 
/// @desc handles the chunks at runtime -> load/delete/create/save chunks
/// @return 

if (!initial_generaton) {

	// followID has moved?
	if (abs(followId.x-lastX) > (CHUNK_SIZE * TILE_SIZE) || abs(followId.y-lastY) > (CHUNK_SIZE * TILE_SIZE) || m_thread.active || init) {
		
		init = false;
		
		// determine the direction
		if ((followId.x-lastX) > CHUNK_SIZE * TILE_SIZE) { centerChunkX++; lastX += CHUNK_SIZE * TILE_SIZE; }
		if ((lastX-followId.x) > CHUNK_SIZE * TILE_SIZE) { centerChunkX--; lastX -= CHUNK_SIZE * TILE_SIZE; }
		if ((followId.y-lastY) > CHUNK_SIZE * TILE_SIZE) { centerChunkY++; lastY += CHUNK_SIZE * TILE_SIZE; }
		if ((lastY-followId.y) > CHUNK_SIZE * TILE_SIZE) { centerChunkY--; lastY -= CHUNK_SIZE * TILE_SIZE; }
								
		// delete (out of scope chunks)
		chunk_checkOutOfScope(centerChunkX, centerChunkY);
		
		#region load/ create Chunks
		if (m_thread.i-- <= 0) {
			
			var _bool;
			 xx = m_thread.active ? m_thread.xx : centerChunkX-CHUNK_RAD;
			 yy = m_thread.active ? m_thread.yy : centerChunkY-CHUNK_RAD;
				
			do  {
									
				if (xx > centerChunkX + CHUNK_RAD) {
					yy++
					xx = centerChunkX-CHUNK_RAD;
					if (yy > centerChunkY + CHUNK_RAD) {
						m_thread.active = false;
						exit;
					}
				
				}									
				
				var _boolean = chunkHandler_getChunkAtCell(xx, yy);
				
				if (_boolean == noone) {
					var _id;
					if (file_exists(CNK_SAV_DIR+string(xx)+"_"+string(yy))) {
						_id = chunk_load(xx, yy);
					}else{
						_id = chunk_generate(xx, yy, NOISE_FREQUENZY, NOISE_PERSISTENT, NOISE_ITERATIONS, NOISE_FREQUENZY_MULTIPLIER, true);
					}
				}
					
				xx++;
				
			}until(_boolean == noone);
				
			m_thread.xx = xx;
			m_thread.yy = yy;
			m_thread.i = M_THREAD_HANDLE_N;	
			m_thread.active = true;
		
		}
	#endregion
	}
}else{	
	__chunkHandler_chunkPreLoad(initial_coordX, initial_coordY);		
}

