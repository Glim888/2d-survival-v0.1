/// @func inventory_changeItemPosition
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @desc swap the position of 2 items
/// @return 

with (obj_logic_inventory) {
	
	var _temp = inv_grid[# argument0 , argument1];
	inv_grid[# argument0 , argument1] = inv_grid[# argument2 , argument3];
	inv_grid[# argument2 , argument3] = _temp;
	
}