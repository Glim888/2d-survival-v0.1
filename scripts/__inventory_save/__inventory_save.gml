/// @func __inventory_save
/// @param 
/// @desc save the complete inventory to disk
/// @return 

var _map = ds_map_create();
var _id = noone
var _numb = 0;

for (var _y=0; _y<inv_h; _y++) {
	for (var _x=0; _x<inv_w; _x++) {
		if (inv_grid[# _x, _y] != noone) {
			_id		= inv_grid[# _x, _y].itemID;
			_numb	= inv_grid[# _x, _y].itemNumber;
		}else{
			_id = noone;
			_numb = 0;
		}
		ds_map_add(_map, "itemID_" + string(_x + _y*inv_w), _id);
		ds_map_add(_map, "itemNumber_" +string(_x + _y*inv_w), _numb);
	}
}


// Save File (do not change anything without care)
var _file = file_text_open_write(INV_SAV_DIR + inv_filename);
file_text_write_string(_file, "DO NOT CHANGE ANYTHING, OR YOUR GAME WILL STOP WORKING!")
file_text_writeln(_file);
file_text_write_real(_file, inv_w);
file_text_writeln(_file);
file_text_write_real(_file, inv_h);
file_text_writeln(_file);
file_text_write_string(_file, base64_encode(json_encode(_map)));
file_text_close(_file);

ds_map_destroy(_map);
