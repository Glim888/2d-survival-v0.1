/// @func __inventory_load
/// @param name of load file
/// @desc load the inventory from disk
/// @return 

if (file_exists(INV_SAV_DIR + argument0)) {

	// Load File (do not change anything without care)
	var _file = file_text_open_read(INV_SAV_DIR + argument0);
	
	if (_file == -1) {
		show_error("File does not exist!", 0); 
		return;
	}
	
	file_text_readln(_file);
	inv_w = file_text_read_real(_file);
	file_text_readln(_file);
	inv_h = file_text_read_real(_file);
	file_text_readln(_file);
	var _map = json_decode(base64_decode(file_text_read_string(_file)));
	file_text_close(_file);


	var _id = noone;
	var _numb = 0;
	var _val;

	for (var _y=0; _y<inv_h; _y++) {
		for (var _x=0; _x<inv_w; _x++) {
	
			_id = _map[? "itemID_"+string(_x + _y*inv_w)];
			_numb = _map[? "itemNumber_"+string(_x + _y*inv_w)];
		
			if (_id != noone) {		
				__inventory_createSlot(_x, _y, _id, _numb);
			}else{
				inv_grid[# _x , _y] = noone;
			}
		}
	}		

	ds_map_destroy(_map);

}else{
	//show_error("There is no file with that name!", 0); // @TODO 
}