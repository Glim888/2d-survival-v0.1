/// @func handlerMO_registerToDrawHp
/// @param sMO Instance
/// @desc add a sMO Instance to "drawHP_list", the handler will draw the hp of that instance then
/// @return 

if (object_is_ancestor(argument0, obj_sMO)) {
	ds_list_add(drawHP_list, argument0);
	return;
}
Log("ERROR-handlerMO_registerToDrawHp");
