/// @func data_inputAmount
/// @param object_index
/// @desc get inputAmount, based on object_index
/// @return prd_inputAmount

array = ERROR;

switch (argument0) {
	
	case obj_mO_bld_prd_mineIron: array[0] = 1; break;
	case obj_mO_bld_prd_furnance: array[0] = 1; break;
		
	default: Log("data_inputAmount - Switch->Default");break;	
}


return array;