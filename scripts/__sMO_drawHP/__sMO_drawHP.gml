/// @func __sMO_drawHP
/// @param 
/// @desc draw healthbar, if the mapObject has lost some hp
/// @return 


if (!sMO_drawHPFlag) return;

draw_healthbar(x- 20, y -5, x+20, y+5, map(sMO_hp, 0, data_Hp(object_index), 0, 100), c_black, c_red, c_green, 0, 0, 1);	
