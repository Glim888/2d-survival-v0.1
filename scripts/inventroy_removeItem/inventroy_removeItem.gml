/// @func inventroy_removeItem
/// @param inventoryId
/// @param itemID
/// @param number
/// @desc decrease the number of that item and destroy the item slot if there are no more elements of that item
/// @return 

with (argument0) {
	
	var _invPosition = __inventory_findItem(argument1);	
	
	if (_invPosition[0] >= 0 ) {
		inv_grid[# _invPosition[0] , _invPosition[1]].itemNumber -= argument2;	
		
		// if there is no more element of that item we remove the item from that slot
		if (inv_grid[# _invPosition[0] , _invPosition[1]].itemNumber <=0) {
			__inventory_destroySlot(_invPosition);
		}
	}
	
}