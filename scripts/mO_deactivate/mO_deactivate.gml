/// @func mO_deactivate
/// @param instance
/// @desc deactivate a map object 
/// @return 

instance_deactivate_object(argument0);