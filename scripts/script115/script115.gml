/// @func inventory_getWidth
/// @param inventoryId
/// @desc get the width of the inventory
/// @return width or ERROR

with (argument0) return inv_w;

Log("wrongArgument! - inventory_getWidth");

return ERROR;