/// @func data_Hp
/// @param object_index
/// @desc get Hp, based on object_index
/// @return hp;


switch (argument0) {
	
	case obj_mO_res_treeSmall: return 150; break;
	case obj_mO_res_treeBig: return 200; break;
	case obj_mO_res_coal: return 200; break;
	case obj_mO_res_iron: return 300; break;
	case obj_mO_res_stone: return 300; break;
	case obj_mO_res_treeStump: return 250; break;	
	default: Log("data_Hp-Default!!!"); return 1; break;
		
}

