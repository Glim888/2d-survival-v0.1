/// @func __inventory_createSlot
/// @param xCell
/// @param yCell
/// @param itemID
/// @param number of elements
/// @desc creates a new inventory slot and add it to the inventory grid
/// @return 

var _inst = instance_create_layer(0, 0, "Logic", obj_logic_inventorySlot);
instance_deactivate_object(_inst);

inv_grid[# argument0 , argument1] = _inst;
inv_grid[# argument0 , argument1].itemID = argument2;
inv_grid[# argument0 , argument1].itemNumber = argument3;