/// @func __sMO_init
/// @param 
/// @desc initialize the static mapObject
/// @return 

depth = MAP_OBJECT_DEPTH_MULTIPLIER * y;
image_speed = 0;
image_index = data_imageIndex(object_index);

sMO_hp = data_Hp(object_index);
sMO_itemID = data_itemID(object_index);
