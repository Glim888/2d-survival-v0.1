/// @func res_resetHarvest
/// @param resource instance
/// @desc reset harvest_hp back to default value
/// @return 

with (argument0)  {
	harvest_hp = __res_getHarvestHp();
	harvest_flag = false;
}