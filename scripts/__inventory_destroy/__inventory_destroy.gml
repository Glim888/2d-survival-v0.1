/// @func __inventory_destroy
/// @param 
/// @desc destroy logic
/// @return 

for (var _y=0; _y<inv_h; _y++) {
	for (var _x=0; _x<inv_w; _x++) {
		__inventory_destroySlot(_x, _y);				
	}	
}