/// @func data_productionTime
/// @param object_index
/// @desc get productionTime, based on object_index
/// @return prd_productionTime

switch (argument0) {
	
		case obj_mO_bld_prd_mineIron: return 1*room_speed;
		case obj_mO_bld_prd_furnance: return 2*room_speed;
		case obj_mO_bld_prd_mineCoal: return 1.5*room_speed;
		case obj_mO_bld_prd_furnanceCoal: return 1*room_speed;
		case obj_mO_bld_prd_furnanceStone: return 3*room_speed;
		case obj_mO_bld_prd_mineStone: return 0.7*room_speed;
		default: show_error("data_productionTime - Switch->Default", true); break;
		
	}