/// @func map
/// @param val
/// @param min1
/// @param max1
/// @param min2
/// @param max2
/// @desc map a value from range one to range to (proportional)
/// @return mapped value

var val = argument0;
var min1 = argument1;
var max1 = argument2;
var min2 = argument3;
var max2 = argument4;

return min2 + (max2-min2) * ((val-min1) / (max1-min1));
