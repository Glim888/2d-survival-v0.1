/// @func __inventory_findItem
/// @param item
/// @desc 
/// @return position in gird or -1 as array

pos[0] = -1;
pos[1] = -1;

for (var _y=0; _y<INV_SIZE; _y++) {
	for (var _x=0; _x<INV_SIZE; _x++) {
		if (inv_grid[# _x, _y] != noone) {
			if (inv_grid[# _x, _y].itemId == argument0) {
				pos[0] = _x;
				pos[1] = _y;
				return pos;				
			}
		}
	}
}
return pos;
