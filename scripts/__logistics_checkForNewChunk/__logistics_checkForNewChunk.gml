/// @func __logistics_checkForNewChunk
/// @param chunkID
/// @desc checks a list for chunkID and returns if it is a new chunk
/// @return is a new chunk ? (bool)

return (ds_list_find_index(usedChunks, argument0) == -1)
