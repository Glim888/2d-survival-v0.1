/// @func __inventory_findItem
/// @param itemID
/// @desc find the position of the specified itemID
/// @return position in inventory gird [as array]

var _array = [], _i = 0;

for (var _y=0; _y<inv_h; _y++) {
	for (var _x=0; _x<inv_w; _x++) {
		if (inv_grid[# _x, _y] != noone) {
			if (inv_grid[# _x, _y].itemID == argument0) {
				_array[_i++] = _x;
				_array[_i++] = _y;
			}
		}
	}
}
return _array;
