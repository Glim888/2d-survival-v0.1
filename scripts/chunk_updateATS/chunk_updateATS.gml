/// @func chunk_updateATS
/// @param chunk id
/// @desc update ats 
/// @return 

with (argument0) ats_rectangle_update(atsRect, grid_tiles, 0, 0, ds_grid_width(grid_tiles), ds_grid_height(grid_tiles));