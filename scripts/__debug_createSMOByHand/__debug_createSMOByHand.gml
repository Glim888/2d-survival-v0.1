/// @func __debug_createSMOByHand
/// @param x
/// @param y
/// @param sMO
/// @desc 
/// @return 

var _chunk = handlerCH_getChunkAtCoord(argument0, argument1);

if (_chunk != ERROR) {
	
	var _cellCoords = chunk_getCellCoords(_chunk);
	
	if (_cellCoords != ERROR) {
	
		var _x = (argument0 - (_cellCoords[0] * TILE_SIZE*CHUNK_SIZE)) div TILE_SIZE;
		var _y = (argument1 - (_cellCoords[1] * TILE_SIZE*CHUNK_SIZE)) div TILE_SIZE;
	
		var _bufferOffset = _y * CHUNK_SIZE + _x;
		sMO_create(argument0, argument1, argument2, _chunk, _bufferOffset, 0);
		chunk_addMapObject(_chunk, _bufferOffset, mapObj_Id.mineCoal);
	}

}




