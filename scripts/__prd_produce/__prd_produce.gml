/// @func __prj_produce
/// @param 
/// @desc production logic
/// @return 

alarm[0] = prd_produceTime;
var _b = true;

if (prd_inputItem[0] != noone) {
	for (var _i=0; _i<array_length_1d(prd_inputItem); _i++) _b &= (inventory_getItemNumber(prd_items, prd_inputItem[_i]) >= prd_inputAmount[_i]);
}


if (_b) {

	for (var _i=0; _i<array_length_1d(prd_inputItem); _i++) {		
		inventory_removeItem(prd_items, prd_inputItem[_i], prd_inputAmount[_i]);		
	}
	inventory_addItem(prd_items, prd_outputItem, 1);		
}