/// @func inventory_getHeight
/// @param inventoryId
/// @desc get the height of the inventory
/// @return height or ERROR

with (argument0) return inv_h;

Log("wrongArgument! - inventory_getHeight");

return ERROR;