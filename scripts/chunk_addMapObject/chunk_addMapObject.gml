/// @func chunk_addMapObject
/// @param chunk
/// @param buffer_offset
/// @param new value
/// @desc change a map object inside of the given chunk
/// @return 


with (argument0) {
	
	if (isInDestroyProcess) return;
	
	var _tileId = argument2 == noone ? 0 : 1;
	
	tilemap_set(tilemap_collision, _tileId, argument1 mod CHUNK_SIZE, argument1 div CHUNK_SIZE);	
		
	buffer_poke(buffer_mapObjects, argument1 * buffer_sizeof(MAPOBJECTS_T), MAPOBJECTS_T, argument2);
	__chunk_save();
	
}