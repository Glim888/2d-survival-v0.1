/// @func chunkHandler_getChunkAtCell
/// @param chunk_xCell
/// @param chunk_yCell
/// @desc checks if there is a chunk at the given cell
/// @return id or noone

// TODO "_"

with (obj_logic_chunkHandler) {
	
	var _id = ds_map_find_value(active_chunks, string(argument0) + "_" +string(argument1));
	
	if (is_undefined(_id)) {
		return noone;
	}else{
		return _id;
	}
}