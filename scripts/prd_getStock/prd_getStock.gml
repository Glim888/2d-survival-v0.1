/// @func prd_getStock
/// @param prd Instance
/// @desc returns the current stock of the output Item
/// @return stocknumber

// @TODO anpassen für Lager etc?

with (argument0) {
	return inventory_getItemNumber(prd_items, prd_outputItem);
}

if (!instance_exists(argument0)) Log ("prd_getStock arg0");

return 0;