/// @func logistics_addAssemblyline
/// @param sMO instance (assemblyLine)
/// @param chunkID
/// @desc register a assemblyLine
/// @return logisticsElement

with (obj_logic_logistics) {
	
	var _inst = instance_create_layer(0, 0, "Logic", obj_logic_logisticsElement);
	ds_list_add(assemblyLines, _inst);

	// init AssemblyLine
	with (_inst) {
	
		isAssemblyLine = true;
		instance = argument0;
		usedCells = __logistics_generateUsedCells(argument0, true);
		chunkID = argument1;

	}	
	instance_deactivate_object(_inst);
	__logistics_updateGridSize();	
	__logistics_updateConnectetBuildings()
	
	ds_map_add(map_assemblyLines, string(_inst.usedCells[| 0]) + "_" + string(_inst.usedCells[| 1]), _inst);
	return _inst;
}
