/// @func __sMO_throwErrorIfWrongCreated
/// @param 
/// @desc throws an error, if instance_create_* was used insted of sMO_create()
/// @return 


if (sMO_errorFlag) show_error("use sMO_create!!!", 1);