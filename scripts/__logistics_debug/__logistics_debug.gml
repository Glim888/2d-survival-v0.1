/// @func logistics_debug
/// @param 
/// @desc debug the mp_grid
/// @return 

with  (obj_logic_logistics) {

	if (event_type != ev_draw) {
		Log("Wrong Event!");
		return;
	}

	mp_grid_draw(grid);

	for (var _i=0; _i<ds_list_size(buildings); _i++) {
		var _inst = buildings[| _i];
		draw_text(_inst.instance.x, _inst.instance.y, string(_i));
		draw_text(_inst.instance.x, _inst.instance.y+15, string(ds_list_size(_inst.connectedLogisticsElements)));
		if (_inst.itemID == item_Id.wood) draw_text(_inst.instance.x, _inst.instance.y+25, string(1));
	
		for (var _j=0; _j<ds_list_size(_inst.connectedLogisticsElements); _j++) {
			draw_arrow(_inst.instance.x, _inst.instance.y, _inst.connectedLogisticsElements[| _j].instance.x, _inst.connectedLogisticsElements[| _j].instance.y, 10);
		}
	}
}