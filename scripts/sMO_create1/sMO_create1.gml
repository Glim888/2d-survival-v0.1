/// @func sMO_create
/// @param x
/// @param y
/// @param object_index
/// @param chunk_id
/// @param buffer_offset
/// @param biome
/// @desc create a Map Object + some creation logic
/// @return mapObject instance id

var _inst = instance_create_layer( argument0, argument1, "Instances", argument2);

handlerMO_registerMapObject(_inst);

with (_inst) {
	
	sMO_chunkID = handlerCH_getChunkAtCoord(argument0, argument1);
	gg = argument3;//argument3; // TODO handlerCH_getChunkAtCoord ? 
	sMO_bufferOffset = argument4;
	sMO_biome = argument5;
	event_user(0);	// custom Create Event
	
}

return _inst;

				