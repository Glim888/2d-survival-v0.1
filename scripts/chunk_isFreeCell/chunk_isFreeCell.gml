/// @func chunk_isFreeCell
/// @param x
/// @param y
/// @desc checks a grid cell for collision
/// @return collision/ no collision


coll = false;

with (obj_logic_chunk) 
	other.coll |= tilemap_get_at_pixel(tilemap_collision, argument0, argument1) == 1;	

return !coll;
