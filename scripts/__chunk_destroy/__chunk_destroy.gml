/// @func __chunk_destroy
/// @param 
/// @desc destroy_logic
/// @return 

__chunk_save();

isInDestroyProcess = true;

if (layer_tilemap_exists(COLLISION_LAYER, tilemap_collision)) layer_tilemap_destroy(tilemap_collision);

var _size = ds_list_size(mapObjects_list);
for (var _i=0; _i<_size; _i++) instance_destroy(mapObjects_list[| _i]); 

handlerCH_deregisterChunk(self);