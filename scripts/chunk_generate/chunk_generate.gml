/// @func chunk_generate
/// @param chunk_xCell
/// @param chunk_yCell
/// @param noise_frequenzy
/// @param noise_persistent
/// @param noise_iterations
/// @param noise_iterations
/// @param initChunk?
/// @desc generate a new vanilla Chunk for the World
/// @return chunk instance


var _chunk = instance_create_layer(argument0, argument1, "Logic", obj_logic_chunk);

with (_chunk) {
	
	noise_temp = noise2D_create(CHUNK_SIZE, global.seed, argument2, argument3, argument4, argument5, argument0 * CHUNK_SIZE, argument1 * CHUNK_SIZE);
	noise_moisture = noise2D_create(CHUNK_SIZE, global.seed*23.456, argument2*2.0, argument3*0.5, argument4, argument5, argument0 * CHUNK_SIZE, argument1 * CHUNK_SIZE);
	noise_mapObjects = noise2D_create(CHUNK_SIZE, global.seed*2345.64, argument2*2., argument3, argument4, argument5*3, argument0 * CHUNK_SIZE, argument1 * CHUNK_SIZE);
	
	
	#region generate buffer_bioms from noise
	
	for (var _y=0; _y<CHUNK_SIZE; _y++) {
		for (var _x=0; _x<CHUNK_SIZE; _x++) {
			buffer_write(buffer_bioms, BIOMS_T, __chunk_getBiome(noise_temp[# _x, _y], noise_moisture[# _x, _y]));
		}
	}
	
	#endregion
	
	#region generate buffer_mapObjects from noise
				
	var _id, _v, _vr, _n=0;
	for (var _y=0; _y<CHUNK_SIZE; _y++) {
		for (var _x=0; _x<CHUNK_SIZE; _x++) {	
			
			_v = noise_mapObjects[# _x, _y];
			_vr = round(_v);
			_id = noone;
			
			if (_n++ mod MAP_OBJECTS_INCREMENT == 0) {	
				#region Wood
				if (_v > 180) {
				
					if (_v > 190) _id = mapObj_Id.tree_big;			
					else if (_v > 181) _id = mapObj_Id.tree_small;		
					else _id = mapObj_Id.tree_stump;						
				
				
				}
				#endregion
				#region Coal, Iron, Stone
				if (_v < 10) {
			
				if (_v > 7 ) _id = mapObj_Id.iron;	
				else if (_v > 4 ) _id = mapObj_Id.coal;	
				else _id = mapObj_Id.stone;
					
				}
				#endregion
				#region Special	
				if (_v > 50 && _v < 150) {
			
					if (_vr == 100) _id = mapObj_Id.tree_stump;
					if (_vr == 99) _id = choose(mapObj_Id.tree_small, mapObj_Id.tree_big);
			
				}	
				#endregion
			}
			buffer_write(buffer_mapObjects, MAPOBJECTS_T, _id);
		}
	}
	
	
	#endregion	
	
	ds_grid_destroy(noise_temp);
	ds_grid_destroy(noise_moisture);

	chunk_doInitaialize = argument6;
	__chunk_save();
	
}

return _chunk;


