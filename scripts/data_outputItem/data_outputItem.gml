/// @func data_outputItem
/// @param object_index
/// @desc get outputItem, based on object_index
/// @return prd_outputItem

switch (argument0) {
	
		case obj_mO_bld_prd_mineIron: return item_Id.iron;
		case obj_mO_bld_prd_furnance: return item_Id.stone;
		case obj_mO_bld_prd_mineCoal: return item_Id.coal;
		case obj_mO_bld_prd_furnanceCoal: return item_Id.iron;
		case obj_mO_bld_prd_furnanceStone: return item_Id.iron;
		case obj_mO_bld_prd_mineStone: return item_Id.iron;
		
		default: show_error("data_outputItem - Switch->Default", true); break;
		
	}