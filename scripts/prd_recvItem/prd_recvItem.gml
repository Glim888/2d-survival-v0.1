/// @func prd_recvItem
/// @param prd Instance
/// @param item instance
/// @desc receive a item
/// @return 

with (argument0) {

	if (argument1.object_index == obj_logic_item) {
		
		var _item = item_getItemId(argument1);
		
		if (_item != ERROR) {
			var _pos = __prd_findInventoryPosition(_item);
			
			if (_pos != ERROR) {
				inventory_addItemAtPosition(prd_items, _item, _pos, 0);	
				prd_openRequests[_pos]--;
			}
		}		
	}
	
}