/// @func inventory_addItem
/// @param item
/// @param number of elements to add
/// @desc add a item to inventory
/// @return 

with (obj_logic_inventory) {
	
	var _invPosition = __inventory_findItem(argument0);

	// item exists in inventory
	if (_invPosition[0] >= 0 ) {
		inv_grid[# _invPosition[0] , _invPosition[1]].itemNumber += argument1;
	}else{
		// create new inventory slot
		_invPosition = __inventory_findFreeSlot();
		inv_grid[# _invPosition[0] , _invPosition[1]] = instance_create_layer(0, 0, "Logic", obj_logic_inventorySlot);
		inv_grid[# _invPosition[0] , _invPosition[1]].itemId = argument0;
		inv_grid[# _invPosition[0] , _invPosition[1]].itemNumber += argument1;
	}	
}