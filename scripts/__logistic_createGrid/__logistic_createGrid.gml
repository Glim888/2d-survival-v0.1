/// @func __logistic_createGrid
/// @param topLeft [Tiles]
/// @param topLeft [Tiles]
/// @param number of h Cells
/// @param number of v Cells
/// @desc create a new mp_grid
/// @return 


grid = mp_grid_create(argument0*TILE_SIZE, argument1*TILE_SIZE, argument2, argument3, TILE_SIZE, TILE_SIZE);

for (var _y=0; _y <argument3; _y++) 
	for (var _x=0; _x<argument2; _x++) 
		mp_grid_add_cell(grid, _x, _y);

// 2 variables, which define the top left corner of the mp_grid
topLeftX = argument0;
topLeftY = argument1;