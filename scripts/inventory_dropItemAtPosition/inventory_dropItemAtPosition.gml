/// @func inventory_dropItemAtPosition
/// @param inventoryId
/// @param x Cell
/// @param y Cell
/// @desc drop all items from this cell
/// @return 

with (argument0) {
	
	if (argument1 < 0 || argument1 > inv_w) show_error("out of bounds(x)", 0);	
	if (argument2 < 0 || argument2 > inv_h) show_error("out of bounds(y)", 0);	
	
	// item_create(inv_grid[# argument1 , argument2].itemNumber);
	inventory_addItem(global.inventory, inv_grid[# argument1 , argument2].itemID, inv_grid[# argument1 , argument2].itemNumber);
	
	instance_destroy(inv_grid[# argument1 , argument2]);	
	inv_grid[# argument1 , argument2] = noone;

}

