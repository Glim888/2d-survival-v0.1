/// @func bld_dislodge
/// @param buildingId
/// @desc dislodge the given building
/// @return 

with (argument0) {
	
	for (var _x=0; _x< 5; _x++) inventory_dropItemAtPosition(items, _x, 0);
	//inventory_addItem(global.inventory, __bld_getItemId(), 1);
	
}