/// @func __logistics_removeFromGrid
/// @param logisticsElement
/// @desc remove used Cells to mp_grid
/// @return 

var _list = argument0.usedCells;
var _len = ds_list_size(_list);

for (var _i=0; _i<= _len-2; _i+=2) mp_grid_add_cell(grid, _list[| _i] - topLeftX, _list[| _i+1] - topLeftY);