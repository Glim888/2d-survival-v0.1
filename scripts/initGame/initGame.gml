/// @func initGame
/// @param 
/// @desc initialize logic for the game
/// @return 

display_set_gui_size(window_get_width(), window_get_height());
application_surface_draw_enable(true);
gml_release_mode(0);

layer_create(COLLISION_LAYER_DEPTH, COLLISION_LAYER);
layer_set_visible(COLLISION_LAYER, false);

#region Create Logic Objects

instance_create_layer(0,0, "Logic", obj_logic_handler);
instance_create_layer(0,0, "Logic", obj_logic_logistics);
instance_create_layer(0,0, "Logic", obj_logic_debug);

#endregion

#region itit ATS
var _priority = ds_priority_create();

ds_priority_add(_priority, spr_example_tile_air, TILE_DEPTH+6);
ds_priority_add(_priority, spr_tile_dessert, TILE_DEPTH+5);
ds_priority_add(_priority, spr_tile_swamp, TILE_DEPTH+4);
ds_priority_add(_priority, spr_tile_gL1, TILE_DEPTH+3);
ds_priority_add(_priority, spr_tile_gL2, TILE_DEPTH+2);
ds_priority_add(_priority, spr_tile_gL3, TILE_DEPTH+1);
ds_priority_add(_priority, spr_tile_snow, TILE_DEPTH+0);

ats_init(TILE_SIZE, TILE_SIZE, _priority, 4);
ds_priority_destroy(_priority);
#endregion

