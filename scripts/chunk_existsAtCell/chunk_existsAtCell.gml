/// @func chunk_existsAtCell
/// @param chunk_xCell
/// @param chunk_yCell
/// @desc checks if there is a chunk at the given cell
/// @return true or false

boolean = false;

with(obj_logic_chunk) other.boolean |= (chunk_xCell == argument0 && chunk_yCell == argument1);

return boolean;