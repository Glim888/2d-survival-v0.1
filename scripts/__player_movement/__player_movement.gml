/// @func __player_movement
/// @param 
/// @desc Player Movement Logic
/// @return 

//TODO Buttons platformunabhängig
if (keyboard_check(BUTTON_UP) || keyboard_check(BUTTON_DOWN) || keyboard_check(BUTTON_LEFT) || keyboard_check(BUTTON_RIGHT)) {
	if (keyboard_check(BUTTON_LEFT)	 && chunk_isFreeCell(x-spd, y)) x -= spd * (delta_time / 16666); //TODO
	if (keyboard_check(BUTTON_RIGHT) && chunk_isFreeCell(x+spd, y)) x += spd * (delta_time / 16666);
	if (keyboard_check(BUTTON_UP)	 && chunk_isFreeCell(x, y-spd)) y -= spd * (delta_time / 16666);
	if (keyboard_check(BUTTON_DOWN)  && chunk_isFreeCell(x, y+spd)) y += spd * (delta_time / 16666);
		
}