/// @func logistics_registerBuilding
/// @param obj_sMO instance
/// @param itemID
/// @param chunkID
/// @desc register a new building
/// @return logisticsElement

with (obj_logic_logistics) {
	
	var _inst = instance_create_layer(0, 0, "Logic", obj_logic_logisticsElement);
	ds_list_add(buildings, _inst);
	
	with (_inst) {
	
		isAssemblyLine = false;
		instance = argument0;
		itemID = argument1;
		usedCells = __logistics_generateUsedCells(argument0, false);
		chunkID = argument2;
		connectedLogisticsElements = ds_list_create();
		path = path_add();

	}
	instance_deactivate_object(_inst);
	__logistics_updateGridSize();
	__logistics_updateConnectetBuildings()
	return _inst;
}