/// @func __item_create
/// @param x
/// @param y
/// @param item_Id
/// @param path or noone
/// @desc 
/// @return 

var _inst = instance_create_layer(argument0, argument1, "Instances", obj_logic_item);

with (_inst) {

	itemID = argument2;
	
	if (argument3 != noone) {
		path = path_add();
		path_assign(path, argument3);
		path_start(path, 1, path_action_stop, 1);
	}
	sprite_index = spr_map_tree_stump;
	
}