/// @func __chunkHandler_setNoiseParameter
/// @desc set the predefined noise parameters
/// @return 

switch (global.noiseType) {
		
	case 0: 
		NOISE_FREQUENZY					= 0.05;
		NOISE_PERSISTENT				= 0.3;
		NOISE_ITERATIONS				= 2;
		NOISE_FREQUENZY_MULTIPLIER		= 9.45;
	break;
	
	case 1: 
		NOISE_FREQUENZY					= .15;
		NOISE_PERSISTENT				= 0.3;
		NOISE_ITERATIONS				= 2;
		NOISE_FREQUENZY_MULTIPLIER		= 2.78;
	break;
	
	case 2: 
		NOISE_FREQUENZY					= 0.25;
		NOISE_PERSISTENT				= 0.4;
		NOISE_ITERATIONS				= 2;
		NOISE_FREQUENZY_MULTIPLIER		= 4.1213;
	break;
	
	case 3: 
		NOISE_FREQUENZY					= 0.6;
		NOISE_PERSISTENT				= 0.5;
		NOISE_ITERATIONS				= 2;
		NOISE_FREQUENZY_MULTIPLIER		= 6.78;
	break;
	
	case 4: 
		NOISE_FREQUENZY					= 0.9;
		NOISE_PERSISTENT				= 0.6;
		NOISE_ITERATIONS				= 2;
		NOISE_FREQUENZY_MULTIPLIER		= 14.1213;
	break;
	
	case 5: 
		NOISE_FREQUENZY					= 1.1;
		NOISE_PERSISTENT				= 1.1;
		NOISE_ITERATIONS				= 2;
		NOISE_FREQUENZY_MULTIPLIER		= 18.1213;
	break;
	
	case 6: 
		NOISE_FREQUENZY					= 0.1;
		NOISE_PERSISTENT				= 0.1;
		NOISE_ITERATIONS				= 2;
		NOISE_FREQUENZY_MULTIPLIER		= 11.1213;
	break;
	
	default: show_error("Wrong noise_type", true); break;
}
	