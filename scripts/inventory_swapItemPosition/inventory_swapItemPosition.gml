/// @func inventory_swapItemPositions
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @desc swap the position of 2 items
/// @return 

with (obj_logic_inventory) {
	
	var _invPosition = __inventory_findItem(argument0);

	// item exists in inventory
	if (_invPosition[0] >= 0 ) {
		inv_grid[# _invPosition[0] , _invPosition[1]].itemNumber += argument1;