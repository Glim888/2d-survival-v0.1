/// @func __logistics_generateUsedCells
/// @param prd Instance
/// @param isAssemblyLine
/// @desc get used cells 
/// @return list of used Cells (x1,y1,x2,y2....)


var _list = ds_list_create();

var _xx = argument0.x div TILE_SIZE;
var _yy = argument0.y div TILE_SIZE;

if (argument1) {
	_list[| 0] = _xx;
	_list[| 1] = _yy;	
}else{
	_list[| 0] = _xx;
	_list[| 1] = _yy;
	_list[| 2] = _xx+1;
	_list[| 3] = _yy;
	_list[| 4] = _xx;
	_list[| 5] = _yy+1;
	_list[| 6] = _xx+1;
	_list[| 7] = _yy+1;
}


return _list;

