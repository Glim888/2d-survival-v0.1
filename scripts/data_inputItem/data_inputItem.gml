/// @func data_inputItem
/// @param object_index
/// @desc get inputItem, based on object_index
/// @return prd_inputItem as Array

inputItem[0] = ERROR; // known BUG, needs a special name

switch (argument0) {
	
		case obj_mO_bld_prd_mineIron: inputItem[0] = noone; break;
		case obj_mO_bld_prd_furnance: inputItem[0] = item_Id.iron; break;
		case obj_mO_bld_prd_mineCoal: inputItem[0] = noone; break;
		case obj_mO_bld_prd_furnanceCoal: inputItem[0] = item_Id.coal; break;
		case obj_mO_bld_prd_furnanceStone: inputItem[0] = item_Id.stone; break;
		case obj_mO_bld_prd_mineStone: inputItem[0] = noone; break;
		
		default: show_error("data_inputItem - Switch->Default", true); break;
		
	}


return inputItem;

