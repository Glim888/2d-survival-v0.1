/// @func handlerCH_getChunkAtCell
/// @param chunk_xCell
/// @param chunk_yCell
/// @desc checks if there is a chunk at the given cell
/// @return id or ERROR

// TODO "_"

with (obj_logic_handler) {
	
	var _id = ds_map_find_value(active_chunks, string(argument0) + "_" +string(argument1));
	
	if (is_undefined(_id)) {
		return ERROR;
	}else{
		return _id;
	}
}