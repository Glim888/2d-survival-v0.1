/// @func data_isActive
/// @param object_index
/// @desc get prd_isActive, based on object_index
/// @return prd_isActive

switch (argument0) {
	
		case obj_mO_bld_prd_mineIron: return false;
		case obj_mO_bld_prd_furnance: return false;
		
		default: Log("data_isActive - Switch->Default");break;
		
	}