/// @func __chunk_initTilesCore
/// @param 
/// @desc create a tilemap (only core)
/// @return 

	
var _val, _spr;
	
buffer_seek(buffer_bioms, buffer_seek_start, 0);	
	
for (var _y=0; _y<CHUNK_SIZE; _y++) {
	for (var _x=0; _x<CHUNK_SIZE; _x++) {
			
		// +1 (offset for clean borders)	
		ds_grid_set(grid_tiles, _x+1, _y+1, __chunk_tileGetSprite(buffer_read(buffer_bioms, BIOMS_T))); 
			
	}
}


var _topChunk	= handlerCH_getChunkAtCell(chunk_xCell, chunk_yCell-1);
var _botChunk	= handlerCH_getChunkAtCell(chunk_xCell, chunk_yCell+1);
var _leftChunk	= handlerCH_getChunkAtCell(chunk_xCell-1, chunk_yCell);
var _rightChunk	= handlerCH_getChunkAtCell(chunk_xCell+1, chunk_yCell);


if (_topChunk != ERROR)		{
	chunk_tileInitEdge(self, _topChunk, tileEdge.top);
	chunk_tileInitEdge(_topChunk, self, tileEdge.bot);	
	chunk_updateATS(_topChunk);
}

if (_botChunk != ERROR)		{
	chunk_tileInitEdge(self, _botChunk, tileEdge.bot);
	chunk_tileInitEdge(_botChunk, self, tileEdge.top);
	chunk_updateATS(_botChunk);
}

if (_leftChunk != ERROR)	{
	chunk_tileInitEdge(self, _leftChunk, tileEdge.left);
	chunk_tileInitEdge(_leftChunk, self, tileEdge.right);
	chunk_updateATS(_leftChunk);
}

if (_rightChunk != ERROR)	{
	chunk_tileInitEdge(self, _rightChunk, tileEdge.right);
	chunk_tileInitEdge(_rightChunk, self, tileEdge.left);
	chunk_updateATS(_rightChunk);
}


chunk_updateATS(self);

	

