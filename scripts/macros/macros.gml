/// @func Constants
/// @param 
/// @desc declaring macros & enums
/// @return 

// TODO MARCOS welche immer konstant bleiben müssen!!!


return;
#region Macros
// View Size
#macro VIEW_W 1000 
#macro VIEW_H VIEW_W/1.77778

// resource density -> for loop increment value (low = more mapObjects)
#macro MAP_OBJECTS_INCREMENT 2

// Tileset size
#macro TILE_SIZE 16

// depth of texture tiles
#macro TILE_DEPTH 2000000

#macro COLLISION_LAYER_DEPTH -10000

// Tilenumber per chunk
#macro CHUNK_SIZE 16

// How many chunks are loaded around the player in every direction (top, right, down, left)
#macro CHUNK_RAD 3

// Initial worldgeneration chunk number in every direction (x and y) 
#macro WORLD_INITIAL_SIZE 10

// chunks are saved here
#macro CNK_SAV_DIR "sav/chunks/"

// inventory gets saved here
#macro INV_SAV_DIR "sav/"
//Initial generation ini file directory

#macro INI_DIR "settings.ini"

// Name of the tilelayers
#macro COLLISION_LAYER "Collision"

// Chunk generation step delay
#macro M_THREAD_HANDLE_N 3
#macro M_THREAD_INIT_N 0

// Defines the random x/y offsets form their defined position
#macro MAP_OBJECTS_RND 2

// Instances deactivation check in stepps
#macro INST_DEAC_CYCLE room_speed *0.3

// all map Objects outside of this rad are deactivated
#macro INST_DEAC_RAD 0.7

// If a distance to a resource is smaller, then we can harvest it
#macro HARVEST_DISTANCE 20

// Inventory size (x and y)
#macro INVENTORY_SIZE 10

// Button (depends on platform)
#macro BUTTON_HARVEST vk_space
#macro BUTTON_UP vk_up
#macro BUTTON_DOWN vk_down
#macro BUTTON_LEFT vk_left
#macro BUTTON_RIGHT vk_right

// buffer_types
#macro MAPOBJECTS_T buffer_s8   // -> buffer_fast is just working with u8
#macro BIOMS_T buffer_u8		// -> buffer_fast is just working with u8
#macro ENTITIES_T buffer_u8		// -> buffer_fast is just working with u8
#endregion
#region Enums

// Biome types
enum bioms {
	desert,
	grassland1,
	grassland2,
	grassland3,
	snow,
	swamp,
};

// Map Objects (is used for saving, because the object_index´s are changing)
enum mapObj_ids {
	tree_small,
	tree_big,
	tree_stump,
	coal,
	stone,
	iron,	
};

// Edgetypes of Chunktiles
enum tileEdge {
	topEdge,
	botEdge,
	leftEdge,
	rightEdge,
};


// Resource Harvest HP
enum res_hp {
	tree_small = 150,
	tree_big = 200,
	tree_stump = 250,
	coal = 200,
	stone = 300,
	iron = 100,	
};

enum sMO_image_indexes {
	
}

// Items in this game
enum itemId {
	wood,
	iron,
	stone,
	coal, 
}

#endregion


