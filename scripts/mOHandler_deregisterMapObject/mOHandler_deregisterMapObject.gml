/// @func mOHandler_deregisterMapObject
/// @param mapObject
/// @desc deregister a map object form mO_list
/// @return

ds_list_delete(obj_logic_mapObjectHandler.mO_list, ds_list_find_index(obj_logic_mapObjectHandler.mO_list, argument0));