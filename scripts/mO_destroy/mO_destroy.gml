/// @func mO_destroy
/// @param instance id 
/// @desc destroy a map Object + some destroy logic
/// @return 

handlerMO_deregisterMapObject(argument0);
	
instance_destroy(argument0);

