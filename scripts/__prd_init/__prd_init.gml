/// @func __prd_init
/// @param 
/// @desc initialize the production Instances
/// @return 


// Production Instance is a active (Tower) or a passivs Instance (Producer)
prd_isActive = data_isActive(object_index);	

// defines the items which is requested [as Array]
prd_inputItem = data_inputItem(object_index);

// defines the amount of item is needed per production/ shoot [as Array]
prd_inputAmount = data_inputAmount(object_index);

// defines the length of the input arrays
prd_inputItemLen = array_length_1d(prd_inputItem);

// defines the item which is offered
prd_outputItem = data_outputItem(object_index);

// defines the production time in steps
prd_productionTime = data_productionTime(object_index);

// inventory of the stacked items
prd_items = inventory_create("i_"+string(x)+"_"+string(y), prd_inputItemLen+1, 1);

// inventory of the stacked buffs
prd_buffs = inventory_create("b_"+string(x)+"_"+string(y), 3, 1); // @TODO BUFF LEN CONST
// handles the logistics

prd_logistics = logistics_registerBuilding(self, prd_outputItem, sMO_chunkID);

// defines the open requests -> if the prd requests a item and gets a success, than this value will increase. After recv the value will decrease
prd_openRequests = array_create(prd_inputItemLen, 0);

// start Routines (Request & Defend & Production)
alarm[0] = 1;
alarm[1] = 1;

// Check for Errors
if (prd_inputItemLen != array_length_1d(prd_inputAmount)) show_error("prd_inputItemLen != array_length_1d(prd_inputAmount)", true);

