/// @func __chunkHandler_chunkPreLoad
/// @param initial_coordX
/// @param initial_coordY
/// @desc Initial chunk generation, to save performance at runtime
/// @return 

	
initial_generaton = true;
	
if (m_thread.i-- <=0) {
		
	if (initial_coordX == noone) {
		initial_coordX = argument0 - WORLD_INITIAL_SIZE * CHUNK_SIZE * TILE_SIZE;
		initial_coordY = argument1 - WORLD_INITIAL_SIZE * CHUNK_SIZE * TILE_SIZE;
	}
		
	// Top Left Edge of the Initial Chunk Load Area
	chunkTopLeftX = argument0 div (CHUNK_SIZE * TILE_SIZE);
	chunkTopLeftY = argument1 div (CHUNK_SIZE * TILE_SIZE);
		
	repeat (WORLD_INITIAL_SIZE) {
		var _x = m_thread.active ? m_thread.xx : chunkTopLeftX;
		var _y = m_thread.active ? m_thread.yy : chunkTopLeftY;
		
		if (_x >= 2*WORLD_INITIAL_SIZE + chunkTopLeftX) {
			if (_y >= 2*WORLD_INITIAL_SIZE + chunkTopLeftY) {
				initial_generaton = false;
				m_thread.active = false;
				m_thread.i = 0;
					
				ini_open(INI_DIR);
				ini_write_real("world", "initialized", true);
				ini_close();
					
				exit;
			}
			_y++;
			_x = chunkTopLeftX;
		}
		
		instance_destroy(chunk_generate(_x, _y, NOISE_FREQUENZY, NOISE_PERSISTENT, NOISE_ITERATIONS, NOISE_FREQUENZY_MULTIPLIER, false));
		_x++;
		
		m_thread.active = true;
		m_thread.xx = _x;
		m_thread.yy = _y;
		m_thread.i = M_THREAD_INIT_N;
	}
				
}
