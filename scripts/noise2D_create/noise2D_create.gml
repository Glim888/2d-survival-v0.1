/// @func noise2D_create
/// @param size					
/// @param seed					
/// @param frequenzy			
/// @param persistent			
/// @param iter					
/// @param frequentyMultipier
/// @param xOffset
/// @param yOffset
/// @desc generate noise
/// @return noise2D_grid between 0-255


var CONST_DEEPTH = 10;

var _size		= argument0;
var _seed		= argument1;
var _freq		= argument2;
var _persi		= argument3;
var _iter		= argument4;
var _freqMul	= argument5;
var _xOffset	= argument6;
var _yOffset	= argument7;

var _posX , _posY, _fractX, _fractY, _val, _mul = 1.0, _xx, _yy, _div, forMaxX = _xOffset + _size, forMaxY = _yOffset + _size;
var _lerp1, _lerp2, lerp3, lerp4;
var _map = ds_grid_create(_size, _size);

// For every octave
for (var _i=1; _i<=_iter; _i++) {
	
	_div = (CONST_DEEPTH div _i);
	// for every grid cell
	for (var _y=_yOffset; _y<forMaxY; _y++) {
		
		// every octave has a other offset (Y)
		_yy = _y * _freq;
		// calc integer and decimal (Y)
		_posY = _y >= 0 ? (_yy div _div) : (_yy div _div) - 1;
		_fractY = _y >= 0 ? frac(_yy / _div) : 1 + (frac(_yy / _div));
		
		for (var _x=_xOffset; _x<forMaxX; _x++) {
				
			// every octave has a other offset (X)
			_xx = _x * _freq;		
			// calc integer and decimal (X)
			_posX = _x >= 0 ? (_xx div _div) : (_xx div _div) - 1;				
			_fractX = _x >= 0 ? frac(_xx / _div) : 1 + (frac(_xx / _div));
							
			// Interpolate between Integers with decimal

			
			_map[# _x-_xOffset, _y-_yOffset] +=	lerp (
								lerp (noise2D_getRandom(_posX, _posY, _seed)  , noise2D_getRandom(_posX, _posY+1, _seed), _fractY),
								lerp (noise2D_getRandom(_posX+1, _posY, _seed), noise2D_getRandom(_posX+1, _posY+1, _seed), _fractY),								
								_fractX) * _mul;
			
			
			
			// Clamp between (-1 and 1)
			if (_i == _iter) _map[# _x-_xOffset, _y-_yOffset] = (clamp(_map[# _x-_xOffset, _y-_yOffset] , -1.0, 1.0) + 1.0) * 127.5; 		

		}
	}
	_freq *= _freqMul;
	_mul *= _persi;
	
}	

return _map;