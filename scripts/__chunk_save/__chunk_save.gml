/// @func __chunk_save
/// @param 
/// @desc save the chunk to disk
/// @return 

Log("save Chunk");
	
if (md5_bioms !=  buffer_md5(buffer_bioms, 0, buffer_get_size(buffer_bioms)) ||
	md5_mapObjects != buffer_md5(buffer_mapObjects, 0, buffer_get_size(buffer_mapObjects)) ||
	md5_entities != buffer_md5(buffer_entities, 0, buffer_get_size(buffer_entities))) {
			
	var _sB = buffer_get_size(buffer_bioms);
	var _sM = buffer_get_size(buffer_mapObjects)
	var _sE = buffer_get_size(buffer_entities)
			
	var _temp = buffer_create(_sB+_sM+_sE, buffer_u8, 1);
					
	buffer_copy(buffer_bioms, 0, _sB, _temp, 0);
	buffer_copy(buffer_mapObjects, 0, _sM, _temp, _sB);
	buffer_copy(buffer_entities, 0, _sE, _temp, _sB+_sM);
						
	buffer_save(_temp, CNK_SAV_DIR+string(chunk_xCell)+"_"+string(chunk_yCell));
	buffer_delete(_temp);
		
	md5_bioms		= buffer_md5(buffer_bioms, 0 , buffer_get_size(buffer_bioms));
	md5_entities	= buffer_md5(buffer_entities, 0 , buffer_get_size(buffer_entities));
	md5_mapObjects	= buffer_md5(buffer_mapObjects, 0 , buffer_get_size(buffer_mapObjects));
		
}

