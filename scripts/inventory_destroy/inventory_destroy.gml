/// @func inventory_destroy
/// @param
/// @desc free memory & destroy logic
/// @return


if (argument0.object_index == obj_logic_inventory) {
	with (argument0) {

		for (var _y=0; _y<inv_h; _y++) {
			for (var _x=0; _x<inv_w; _x++) {

				array[0] = _x;
				array[1] = _y;
				__inventory_destroySlot(array);
				
			}	
		}
		ds_grid_destroy(inv_grid);
		instance_destroy(argument0);

	}
}