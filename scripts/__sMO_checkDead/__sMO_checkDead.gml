/// @func __sMO_checkDead
/// @param 
/// @desc checks the current hp, and destroys that instance, if nessecary
/// @return 

if (sMO_hp <= 0) {

	instance_destroy();
	
}