/// @func data_noisePersistent
/// @param noiseType
/// @desc return noisePersistent based on noiseType
/// @return noisePersistent

switch (argument0) {
	
	case 0: return 0.3;
	case 1: return 0.3;
	case 2: return 0.4;
	case 3: return 0.5;
	case 4: return 0.6;
	case 5: return 1.1;
	case 6: return 0.1;
	default: show_error("wrong Argument", 1);
	
}