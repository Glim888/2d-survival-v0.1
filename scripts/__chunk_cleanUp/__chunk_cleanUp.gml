/// @func __chunk_cleanUp
/// @desc free memory
/// @return


buffer_delete(buffer_bioms);
buffer_delete(buffer_mapObjects);
buffer_delete(buffer_entities);
	
ds_list_destroy(mapObjects_list);
ds_grid_destroy(grid_tiles);
instance_destroy(atsRect);




