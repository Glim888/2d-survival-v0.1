/// @func data_noiseIterations
/// @param noiseIterations
/// @desc return noiseIterations based on noiseType
/// @return noiseIterations

switch (argument0) {
	
	case 0: return 2;
	case 1: return 2;
	case 2: return 2;
	case 3: return 2;
	case 4: return 2;
	case 5: return 2;
	case 6: return 2;
	default: show_error("wrong Argument", 1);
	
}