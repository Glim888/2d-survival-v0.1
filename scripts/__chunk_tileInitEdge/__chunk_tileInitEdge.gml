/// @func __chunk_initTilesEdge
/// @param chunk to edit
/// @param neightbor chunk
/// @param tileEdge
/// @desc initialize the edges of a chunk
/// @return 


var _buffer = argument1.buffer_bioms;

with (argument0) {

	switch (argument2) {
	
		case tileEdge.top: 	
			#region topEdge
			if (ats_top) return;
			ats_top = true;
			buffer_seek(_buffer, buffer_seek_start, (CHUNK_SIZE-1)*CHUNK_SIZE);
			for (var _x=0; _x<CHUNK_SIZE; _x++) {						
				ds_grid_set(grid_tiles, _x+1, 0, __chunk_tileGetSprite(buffer_read(_buffer, BIOMS_T)));
			
			}
			#endregion
		break;	
		case tileEdge.bot:
			#region botEdge
			if (ats_bot) return;
			ats_bot = true;
			buffer_seek(_buffer, buffer_seek_start, 0);
			for (var _x=0; _x<CHUNK_SIZE; _x++) {
				ds_grid_set(grid_tiles, _x+1, CHUNK_SIZE+1, __chunk_tileGetSprite(buffer_read(_buffer, BIOMS_T)));					
			}
			#endregion
		break;	
		case tileEdge.left: 
			#region leftEdge
			if (ats_left) return;
			ats_left = true;
			buffer_seek(_buffer, buffer_seek_start, 0);
			for (var _y=0; _y<CHUNK_SIZE; _y++) {						
				ds_grid_set(grid_tiles, 0, 1+_y, __chunk_tileGetSprite(buffer_peek(_buffer,  CHUNK_SIZE*_y + CHUNK_SIZE-1, BIOMS_T)));
			
			}
			#endregion
		break;	
		case tileEdge.right: 
			#region rightEdge
			if (ats_right) return;
			ats_right = true;
			buffer_seek(_buffer, buffer_seek_start, 0);
			for (var _y=0; _y<CHUNK_SIZE; _y++) {						
				ds_grid_set(grid_tiles, CHUNK_SIZE+1, 1+_y, __chunk_tileGetSprite(buffer_peek(_buffer,  CHUNK_SIZE*_y, BIOMS_T)));
			
			}
			#endregion
		break;	
		default: show_error("__chunk_initTilesEdges", 0); break;
	
	}

}
