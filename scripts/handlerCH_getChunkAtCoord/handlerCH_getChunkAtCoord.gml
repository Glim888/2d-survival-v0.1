/// @func handlerCH_getChunkAtCell
/// @param X Position
/// @param Y Position
/// @desc checks if there is a chunk at the given coord
/// @return id or ERROR

var _x = argument0 div (TILE_SIZE*CHUNK_SIZE);
var _y = argument1 div (TILE_SIZE*CHUNK_SIZE);
	
if (argument0 < 0) _x--;
if (argument1 < 0) _y--;

return handlerCH_getChunkAtCell(_x, _y);