/// @func __prd_production
/// @param 
/// @desc production logic
/// @return 

alarm[0] = prd_productionTime;
var _b = true;

// check item available
if (prd_inputItem[0] != noone) for (var _i=0; _i<prd_inputItemLen; _i++) _b &= (inventory_getItemNumber(prd_items, prd_inputItem[_i]) >= prd_inputAmount[_i]);
	

if (_b) {
	if (inventory_addItemAtPosition(prd_items, prd_outputItem, __prd_findInventoryPosition(prd_outputItem), 0)) {
		for (var _i=0; _i<prd_inputItemLen; _i++) {		
			inventory_removeItem(prd_items, prd_inputItem[_i], prd_inputAmount[_i]);		
		}
	}else{
		// show_hint(inv_full);	
	}
}