/// @func __chunk_updateNeighborTiles
/// @param biome
/// @desc returns the right sprite based on the biome type
/// @return sprite

switch (argument0) {
	
	case bioms.snow: return spr_tile_swamp; break;
	case bioms.swamp: return spr_tile_swamp; break;
	case bioms.desert: return spr_tile_dessert; break;
	case bioms.grassland1: return spr_tile_gL1; break;
	case bioms.grassland2: return spr_tile_gL2; break;
	case bioms.grassland3: return spr_tile_gL3; break;
	default: show_error("wrong biome type", 0); break;
	
}



