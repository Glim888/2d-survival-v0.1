/// @func logistics_requestItem
/// @param logisticsElement
/// @param item_Id
/// @desc finds the building, which meet all conditions and start sending
/// @return success or not

with (obj_logic_logistics) {
	
	if (ds_list_find_index(buildings, argument0) != -1) {
		
		var _len = ds_list_size(argument0.connectedLogisticsElements);
		path_clear_points(bestPath); path_clear_points(tempPath);
		var _bestLen = 10000000, _tempLen, _b, _x = argument0.usedCells[| 0]*TILE_SIZE, _y = argument0.usedCells[| 1]*TILE_SIZE, _inst, _foundPath = false, _bestInst = noone;
		__logistics_addToGrid(argument0);
		
		for (var _i=0; _i< _len; _i++) {
			_inst = argument0.connectedLogisticsElements[| _i];
			if (_inst.itemID == argument1 && prd_getStock(_inst.instance)) {
				__logistics_addToGrid(_inst);
				_b = mp_grid_path(grid, tempPath, _x, _y, _inst.usedCells[| 0]*TILE_SIZE, _inst.usedCells[| 1]*TILE_SIZE, 0);
				__logistics_removeFromGrid(_inst);
				
				if (_b) {
					_tempLen = path_get_length(tempPath);
					if (_tempLen < _bestLen) {
						_bestLen = _tempLen;
						_bestInst = _inst;
						path_assign(bestPath, tempPath);
						_foundPath = true;
					}
				}
			}
		
		}
		__logistics_removeFromGrid(argument0);
		
		if (_foundPath) {
			#region Calculate Speed
			var _x, _y, _aL, _key, _speed, _len = path_get_number(bestPath);
			path_reverse(bestPath);
			path_clear_points(_bestInst.path);
			for (var _j=0; _j<_len; _j++) {
				_x = path_get_point_x(bestPath, _j);
				_y = path_get_point_y(bestPath, _j);
				
				_key = string(_x div TILE_SIZE) + "_" + string(_y div TILE_SIZE);
				
				_aL = ds_map_find_value(map_assemblyLines, _key);
				
				if (is_undefined(_aL)){
					_speed = 100; 
				}else{
					_speed = assemblyLine_getSpeed(_aL.instance);
				}
				
				if (_j == 0 || _j == _len-1) {
					_x += TILE_SIZE;
					_y += TILE_SIZE;
				}
				path_add_point(_bestInst.path, _x, _y, _speed);
			}
			#endregion
			
			path_set_closed(_bestInst.path, false);
			
			if (prd_sendItem(_bestInst.instance, _bestInst.path)) return true;
			
		}
	}
}

return false;
