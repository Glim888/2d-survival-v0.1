/// @func chunkHandler_registerChunk
/// @param chunk
/// @desc register a chunk in "active_chunks" map
/// @return 

with (obj_logic_chunkHandler) {

	if (instance_exists(argument0)) {	
		ds_map_add(active_chunks, string(argument0.chunk_xCell)+"_"+string(argument0.chunk_yCell), argument0);	
	}else{
		show_error("chunkHandler_registerChunk chunk does not exist", 0);	
	}

}