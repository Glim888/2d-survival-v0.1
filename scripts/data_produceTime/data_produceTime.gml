/// @func data_produceTime
/// @param object_index
/// @desc get productionTime, based on object_index
/// @return prd_productionTime

switch (argument0) {
	
		case obj_mO_bld_prd_mineIron: return 2*room_speed;
		case obj_mO_bld_prd_furnance: return 0.3*room_speed;
		
		default: Log("data_produceTime - Switch->Default");break;
		
	}