/// @func __logistics_update
/// @param 
/// @desc 
/// @return 


var _len = ds_list_size(buildings);
var _path = path_add();
var _inst1, _inst2;

for (var _i=0; _i<_len; _i++) ds_list_clear(buildings[| _i].connectedLogisticsElements);

for (var _i=0; _i<_len; _i++) {
	_inst1 = buildings[| _i];
	
	for (var _j=_i+1; _j<_len; _j++) { // TODO
			
		if (_i  != _j) {
			 _inst2 = buildings[| _j];
			if (mp_grid_path(grid, _path, _inst1.usedCells[| 0]*TILE_SIZE, _inst1.usedCells[| 1]*TILE_SIZE, _inst2.usedCells[| 0]*TILE_SIZE, _inst2.usedCells[| 1]*TILE_SIZE, 0)) {
				ds_list_add(buildings[| _i].connectedLogisticsElements, buildings[| _j]);
				ds_list_add(buildings[| _j].connectedLogisticsElements, buildings[| _i]);
			}
		}
	}

}
path_delete(_path);