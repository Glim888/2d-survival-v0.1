/// @func __chunk_initTilesEdges
/// @param fitting Chunk
/// @param tileEdge
/// @desc 
/// @return 




if (event_type != ev_alarm) show_error("wrong event", 0);

var _buffer = argument0.buffer_bioms;

switch (argument1) {
	
	case tileEdge.topEdge: 
		
		buffer_seek(_buffer, buffer_seek_start, (CHUNK_SIZE-1)*CHUNK_SIZE);
		for (var _x=0; _x<CHUNK_SIZE; _x++) {
			
			_val = buffer_read(_buffer, BIOMS_T);
			
			if (_val == bioms.snow) _spr = spr_tile_swamp;
			if (_val == bioms.swamp) _spr = spr_tile_swamp;
			if (_val == bioms.desert) _spr = spr_tile_dessert;
			if (_val == bioms.grassland1) _spr = spr_tile_gL1
			if (_val == bioms.grassland2) _spr = spr_tile_gL2;
			if (_val == bioms.grassland3) _spr = spr_tile_gL3;					
			ds_grid_set(grid_tiles, _x+1, 0, _spr);
			
		}
		
	break;	
	case tileEdge.botEdge:
	
		buffer_seek(_buffer, buffer_seek_start, 0);
		for (var _x=0; _x<CHUNK_SIZE; _x++) {
						
			_val = buffer_read(_buffer, BIOMS_T);
			
			if (_val == bioms.snow) _spr = spr_tile_swamp;
			if (_val == bioms.swamp) _spr = spr_tile_swamp;
			if (_val == bioms.desert) _spr = spr_tile_dessert;
			if (_val == bioms.grassland1) _spr = spr_tile_gL1
			if (_val == bioms.grassland2) _spr = spr_tile_gL2;
			if (_val == bioms.grassland3) _spr = spr_tile_gL3;
											
			ds_grid_set(grid_tiles, _x+1, CHUNK_SIZE+1, _spr);		
			
		}
	
	break;	
	case tileEdge.leftEdge: 
	
	break;	
	case tileEdge.rightEdge: 
	
	break;	
	default: show_error("__chunk_initTilesEdges", 0); break;
	
}

