/// @func data_noiseFrequenzyMultiplier
/// @param noiseType
/// @desc return noiseFrequenzyMultiplier based on noiseType
/// @return noiseFrequenzyMultiplier

switch (argument0) {
	
	case 0: return 9.45;
	case 1: return 2.78;
	case 2: return 4.1213;
	case 3: return 6.78;
	case 4: return 14.1213;
	case 5: return 18.1213;
	case 6: return 11.1213;
	default: show_error("wrong Argument", 1);
	
}