/// @func inventory_debug
/// @param inventoryId
/// @param x
/// @param y
/// @param cellSize
/// @desc debug grid
/// @return 

with (argument0) {

	var _id, _size = argument3, _offset = 100;	
	for (var _y=0; _y<inv_h; _y++) {
		for (var _x=0; _x<inv_w; _x++) {
			
			_id = inv_grid[# _x, _y];			
			if (_id != noone) {
			
				draw_text(argument1+_x*_size, argument2+_y*_size, "ID:" + string(_id.itemID));
				draw_text(argument1+_x*_size, argument2+_y*_size+10, "n:" + string(_id.itemNumber));
			}else{
				draw_text(argument1+_x*_size, argument2+_y*_size, "x");
			}
		}
	}
}
	
