/// @func logistics_register
/// @param obj_sMO instance
/// @param itemID
/// @param usedCells as List in Tiles
/// @param chunkID
/// @desc register a new building to logistics
/// @return logisticsElement


inst = instance_create_layer(0,0, "Logic", obj_logic_logisticsElement);

with (inst) {
	
	isAssemblyLine = false;
	instance = argument0;
	itemID = argument1;
	usedCells = argument2; // [in Tiles as List]
	chunkID = argument3;

}

with (obj_logic_logistics) { // TODO
	
	ds_list_add(elements, other.inst);

	if (!__logistics_chunkExists(argument3)) {
		ds_list_add(usedChunks, argument3);
		__logistics_updateGridSize();
	}
	
	__logistics_fillGrid(other.inst);
	// updateElements();

}