/// @func __player_harvest
/// @param 
/// @desc Player Harvest Logic
/// @return

// start harvesting
if (keyboard_check_pressed(BUTTON_HARVEST)) {

	var _id = instance_nearest(x, y, obj_mO_res);
	if (point_distance(_id.x, _id.y, x, y) < HARVEST_DISTANCE) curr_harvestInst = _id;

}

if (curr_harvestInst != noone) {
	
	// cancel harvesting
	if (!instance_exists(curr_harvestInst) || !keyboard_check(BUTTON_HARVEST) || (point_distance(curr_harvestInst.x, curr_harvestInst.y, x, y) > HARVEST_DISTANCE)) {
		curr_harvestInst = noone;
	}else{
		res_addDamage(curr_harvestInst, 1);
	}
	
	
	
}