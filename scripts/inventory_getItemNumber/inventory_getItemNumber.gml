/// @func inventory_getItemNumber
/// @param inventoryId
/// @param itemID
/// @desc returns the current number of the specified item
/// @return number of elements

with (argument0) {
	
	var _slots = __inventory_findItem(argument1);
	var _numb = 0;

	for (var _i=0; _i<array_length_1d(_slots); _i+=2) {
		_numb += inv_grid[# _slots[_i] , _slots[_i+1]].itemNumber;
	}
	
	return _numb;
}

return 0;