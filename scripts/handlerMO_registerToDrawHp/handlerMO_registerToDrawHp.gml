/// @func handlerMO_registerToDrawHp
/// @param sMO Instance
/// @desc add a sMO Instance to "drawHP_list", the handler will draw the hp of that instance then
/// @return 

with (obj_logic_handler) {

	if (object_is_ancestor(argument0.object_index, obj_sMO)) {
		ds_list_add(drawHP_list, argument0);
		return;
	}
	Log("ERROR-handlerMO_registerToDrawHp");

}