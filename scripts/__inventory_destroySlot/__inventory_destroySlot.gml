/// @func __inventory_destroySlot
/// @param xCell
/// @param yCell
/// @desc destroys an inventory Slot
/// @return 

if (inv_grid[# argument0 , argument1] != noone) {
	instance_activate_object(inv_grid[# argument0 , argument1]);
	instance_destroy(inv_grid[# argument0 , argument1]);	
	inv_grid[# argument0 , argument1] = noone;
}