/// @func inventory_getFileName
/// @param inventoryId
/// @desc get filename of current inventory
/// @return filename

with (argument0) return inv_filename;