/// @func chunk_getCellCoords
/// @param chunk id
/// @desc get the cell coords of the given chunk as array
/// @return array with cell coords or ERROR

with (argument0) {
	coords[0] = chunk_xCell;
	coords[1] = chunk_yCell;
	return coords;
}

return ERROR;