/// @func chunk_load
/// @param chunk_xCell
/// @param chunk_yCell
/// @desc load all saved Chunk variables from disk into RAM
/// @return chunk

var _chunk = instance_create_layer(argument0, argument1, "Logic", obj_logic_chunk);

with (_chunk) {

	var _sB = buffer_get_size(buffer_bioms);
	var _sM = buffer_get_size(buffer_mapObjects)
	var _sE = buffer_get_size(buffer_entities)

	var _temp = buffer_load(CNK_SAV_DIR+string(chunk_xCell)+"_"+string(chunk_yCell));
	buffer_copy(_temp, 0, _sB, buffer_bioms, 0);
	buffer_copy(_temp, _sB, _sM, buffer_mapObjects, 0);
	buffer_copy(_temp, _sB+_sM, _sE, buffer_entities, 0);
	buffer_delete(_temp);

}

return _chunk;
