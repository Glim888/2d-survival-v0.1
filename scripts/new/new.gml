/// @func new();
/// @desc create a new container instance
/// @return idOfEmptyInstance

var _inst = instance_create_layer(0, 0, "Logic", obj_logic_object);

instance_deactivate_object(_inst);

return _inst;