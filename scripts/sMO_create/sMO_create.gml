/// @func sMO_create
/// @param x
/// @param y
/// @param object_index
/// @param chunk_id
/// @param buffer_offset
/// @param biome
/// @desc create a Map Object + some creation logic
/// @return mapObject instance id

var _inst = instance_create_layer( argument0, argument1, "Instances", argument2);



with (_inst) {
	
	sMO_chunkID = argument3
	sMO_bufferOffset = argument4;
	sMO_biome = argument5	
	event_user(0);	// custom Create Event
	
}


return _inst;

				