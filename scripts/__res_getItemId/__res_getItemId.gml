/// @func __res_getItemId
/// @param 
/// @desc returns the itemId based on object_index
/// @return itemId


switch (object_index) {
	
	case obj_mO_res_treeSmall: return itemId.wood; break;
	case obj_mO_res_treeBig: return itemId.wood; break;
	case obj_mO_res_coal: return itemId.coal; break;
	case obj_mO_res_iron: return itemId.iron; break;
	case obj_mO_res_stone: return itemId.stone; break;
	case obj_mO_res_treeStump: return itemId.wood; break;
	default: show_error("Switch index", 1); break;
		
}
