/// @func __res_harvestLogic
/// @param 
/// @desc is doing the harvest logic (step event)
/// @return 


if (harvest_flag) {

	harvest_flag = keyboard_check(vk_space);
	
	if (harvest_hp-- < 0) {
		chunk_addMapObject(chunk_id, buffer_offset, noone);
		mO_destroy(self);
		inventory_addItem(choose(itemId.coal, itemId.iron , itemId.stone, itemId.wood), 3);
	}
	
}else{
	harvest_hp = __res_getHarvestHp();	
}