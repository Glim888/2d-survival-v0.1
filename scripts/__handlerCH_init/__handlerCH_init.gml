/// @func __chunkHandler_init
/// @param followId
/// @desc initialize chunkHandler
/// @return


NOISE_FREQUENZY = data_noiseFrequenzy(global.noiseType);
NOISE_PERSISTENT = data_noisePersistent(global.noiseType);
NOISE_ITERATIONS = data_noiseIterations(global.noiseType);
NOISE_FREQUENZY_MULTIPLIER = data_noiseFrequenzyMultiplier(global.noiseType);

followId = argument0;	
	
// Helper vars
lastX = followId.x + TILE_SIZE*CHUNK_SIZE*0.5;
lastY = followId.y + TILE_SIZE*CHUNK_SIZE*0.5;
centerChunkX = 0;
centerChunkY = 0;
initial_generaton = false;
initial_coordX = noone;
initial_coordY = noone;
init = true;

active_chunks = ds_map_create();

m_thread = new();

m_thread.active = false;
m_thread.i = 0;
m_thread.xx = 0;
m_thread.yy = 0;

// TODO INI DIR
ini_open(INI_DIR);
if (!ini_read_real("world", "initialized", false)) {	
		//__handlerCH_chunkPreLoad(obj_player.x, obj_player.y);	
}
ini_close();


