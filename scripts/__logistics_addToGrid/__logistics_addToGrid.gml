/// @func __logistics_addToGrid
/// @param logisticsElement
/// @desc add used Cells to mp_grid
/// @return 

var _list = argument0.usedCells;
var _len = ds_list_size(_list);

for (var _i=0; _i<= _len-2; _i+=2) mp_grid_clear_cell(grid, _list[| _i] - topLeftX, _list[| _i+1] - topLeftY);