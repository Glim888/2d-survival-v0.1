/// @func __debug_drawEnd
/// @param 
/// @desc is debuging the game by drawing values etc into the game
/// @return 

if (global.debug) {
	if (!benchmark) {
		draw_set_color(c_black);
	
		logistics_debug();
	
		with (obj_logic_chunk) {	
			draw_rectangle(chunk_xCell*TILE_SIZE*CHUNK_SIZE, chunk_yCell*TILE_SIZE*CHUNK_SIZE, (1+chunk_xCell)*TILE_SIZE*CHUNK_SIZE, (1+chunk_yCell)*TILE_SIZE*CHUNK_SIZE, 1)
			draw_text(chunk_xCell*TILE_SIZE*CHUNK_SIZE, chunk_yCell*TILE_SIZE*CHUNK_SIZE, string(chunk_xCell) + " " + string(chunk_yCell));		
			if (!ats_top) draw_circle(chunk_xCell*256+128, chunk_yCell*256, 10, 0);
			if (!ats_bot) draw_circle(chunk_xCell*256+128, chunk_yCell*256+256, 10, 0);
			if (!ats_left) draw_circle(chunk_xCell*256, chunk_yCell*256+128, 10, 0);
			if (!ats_right) draw_circle(chunk_xCell*256+128, chunk_yCell*256+256, 10, 0);
		}
		draw_circle(obj_logic_handler.lastX, obj_logic_handler.lastY, 10, 0);
		draw_text(obj_player.x, obj_player.y, string(obj_player.x) + "/" + string(obj_player.y))
	}
}


