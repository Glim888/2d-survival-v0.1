/// @func __chunk_init()
/// @param 
/// @desc create a new Chunk Instance + declare / initialize all Instance Variables
/// @return

depth = TILE_DEPTH;

// Chunk Cell Position
chunk_xCell  = x;
chunk_yCell  = y;
chunk_doInitaialize = true;
	
// Storage of Chunk Data
buffer_bioms = buffer_create(CHUNK_SIZE * CHUNK_SIZE * buffer_sizeof(BIOMS_T), buffer_fast, 1);
buffer_mapObjects  = buffer_create(CHUNK_SIZE * CHUNK_SIZE * buffer_sizeof(MAPOBJECTS_T), buffer_fixed, 1);
buffer_entities = buffer_create(CHUNK_SIZE * CHUNK_SIZE * buffer_sizeof(ENTITIES_T), buffer_fast, 1);
tilemap_collision = layer_tilemap_create(COLLISION_LAYER, chunk_xCell * CHUNK_SIZE * TILE_SIZE, chunk_yCell * CHUNK_SIZE * TILE_SIZE, ts_collision, CHUNK_SIZE, CHUNK_SIZE);
grid_tiles = ds_grid_create(CHUNK_SIZE+2, CHUNK_SIZE+2);	// +2 for clean borders
atsRect = ats_rectangle_create(ds_grid_width(grid_tiles), ds_grid_height(grid_tiles), 0);
ats_left = false
ats_right = false;
ats_top = false;
ats_bot = false;
md5_bioms		= "0000";
md5_mapObjects	= "0000";
md5_entities	= "0000";
isInDestroyProcess = false;
	
mapObjects_list = ds_list_create();

handlerCH_registerChunk(self);


