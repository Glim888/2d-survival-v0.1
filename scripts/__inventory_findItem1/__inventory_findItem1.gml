/// @func __inventory_findItem
/// @param itemID
/// @desc find the position of the specified itemID or -1
/// @return position in inventory gird or -1 [as array]

pos[0] = ERROR;
pos[1] = ERROR;

for (var _y=0; _y<inv_h; _y++) {
	for (var _x=0; _x<inv_w; _x++) {
		if (inv_grid[# _x, _y] != noone) {
			if (inv_grid[# _x, _y].itemID == argument0) {
				pos[0] = _x;
				pos[1] = _y;
				return pos;				
			}
		}
	}
}
return pos;
