/// @func data_noiseFrequenzy
/// @param noiseType
/// @desc return noiseFrequenzy based on noiseType
/// @return noiseFrequenzy

switch (argument0) {
	
	case 0: return 0.05;
	case 1: return 0.15;
	case 2: return 0.25;
	case 3: return 0.6;
	case 4: return 0.9;
	case 5: return 1.1;
	case 6: return 0.1;
	default: show_error("wrong Argument", 1);
	
}