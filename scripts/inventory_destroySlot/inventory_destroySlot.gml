/// @func __inventory_destroySlot
/// @param _invPosition [as array]
/// @desc destroys a inventory Slot
/// @return 


instance_activate_object(inv_grid[# argument0[0] , argument0[1]]);
instance_destroy(inv_grid[# argument0[0] , argument0[1]]);	
inv_grid[# argument0[0] , argument0[1]] = noone;