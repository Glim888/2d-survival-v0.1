/// @func inventory_create
/// @param Save File Name
/// @param inventory width
/// @param inventory height
/// @desc init inventory
/// @return inventoryId

var _inst = instance_create_layer(0, 0, "Logic", obj_logic_inventory);

with (_inst) {

	inv_grid = ds_grid_create(argument1, argument2);
	inv_w = argument1;
	inv_h = argument2;
	// argument1 in inventory id speichern und in allen methoden abfragen!; testen; @desc; 
	inv_filename = argument0;
	ds_grid_add_region(inv_grid, 0, 0, inv_w, inv_h, noone);

	// TODO FILE Name vom user bestimmen lassen
	__inventory_load(argument0);

}

return _inst;
