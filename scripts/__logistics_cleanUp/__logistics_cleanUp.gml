/// @func __logistics_cleanUp
/// @param 
/// @desc free memory
/// @return 

mp_grid_destroy(grid);
ds_list_destroy(buildings);
ds_list_destroy(assemblyLines);
ds_map_destroy(map_assemblyLines);

