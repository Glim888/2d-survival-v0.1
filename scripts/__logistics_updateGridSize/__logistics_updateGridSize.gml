/// @func __logistics_updateGridSize
/// @param 
/// @desc updates the current grid
/// @return 

var _lenE = ds_list_size(buildings);
var _lenA = ds_list_size(assemblyLines);

#region get Borders
var _minX = 100000, _maxX = -100000, _minY = 100000, _maxY = -100000, _coords;

for (var _i=0; _i< _lenE; _i++) {
	
	_coords = chunk_getCellCoords(buildings[| _i].chunkID);
	if (_coords != ERROR) {
		
		if (_coords[0] < _minX) _minX = _coords[0];
		if (_coords[0] > _maxX) _maxX = _coords[0];
		if (_coords[1] < _minY) _minY = _coords[1];
		if (_coords[1] > _maxY) _maxY = _coords[1];
		
	}
}
_coords = ERROR;
for (var _i=0; _i< _lenA; _i++) {
	
	_coords = chunk_getCellCoords(assemblyLines[| _i].chunkID);
	if (_coords != ERROR) {
		
		if (_coords[0] < _minX) _minX = _coords[0];
		if (_coords[0] > _maxX) _maxX = _coords[0];
		if (_coords[1] < _minY) _minY = _coords[1];
		if (_coords[1] > _maxY) _maxY = _coords[1];
		
	}
}
#endregion

var _diffX = _maxX - _minX + 1;
var _diffY = _maxY - _minY + 1;

mp_grid_destroy(grid);
__logistic_createGrid(_minX*CHUNK_SIZE, _minY*CHUNK_SIZE, _diffX*CHUNK_SIZE, _diffY*CHUNK_SIZE);


for (var _i=0; _i< _lenA; _i++) __logistics_addToGrid(assemblyLines[| _i]);
