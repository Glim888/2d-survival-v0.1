/// @func inventory_changeItemPosition
/// @param inventoryId
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @desc change item position in the given inventory
/// @return 

with (argument0) {
	
	var _temp = inv_grid[# argument1 , argument2];
	inv_grid[# argument1 , argument2] = inv_grid[# argument3 , argument4];
	inv_grid[# argument3 , argument4] = _temp;
	
}