/// @func __mOHandler_deactivateFarMapObjects 
/// @param 
/// @desc deactivate all mapObjects, that are out of view --- activate all mapObjects that are in view
/// @return 

//TODO alarm muss konstant sein

if (event_type != ev_alarm) show_error("wrong event", 0);

alarm[0] = INST_DEAC_CYCLE;

var _size = ds_list_size(mO_list);
var _id;

for (var _i=0; _i < _size; _i++) {
	_id = mO_list[| _i];
	
	if (!point_in_rectangle(_id.x, _id.y, obj_player.x - VIEW_W*INST_DEAC_RAD, obj_player.y - VIEW_H*INST_DEAC_RAD,
										  obj_player.x + VIEW_W*INST_DEAC_RAD, obj_player.y + VIEW_H*INST_DEAC_RAD)) {
		mO_deactivate(_id);				 
	}else{
		mO_activate(_id);		
	}
}