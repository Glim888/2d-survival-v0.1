/// @func __debug_step
/// @param 
/// @desc  is debuging the game by hadleing some logic & set some values
/// @return 


if (keyboard_check_released(ord("Y"))) __debug_createSMOByHand(mouse_x, mouse_y, obj_mO_bld_prd_mineIron);
if (keyboard_check_released(ord("X"))) __debug_createSMOByHand(mouse_x, mouse_y, obj_mO_bld_prd_mineCoal);
if (keyboard_check_released(ord("C"))) __debug_createSMOByHand(mouse_x, mouse_y, obj_mO_bld_prd_mineStone);
if (keyboard_check_released(ord("V"))) __debug_createSMOByHand(mouse_x, mouse_y, obj_mO_bld_prd_furnance);
if (keyboard_check_released(ord("B"))) __debug_createSMOByHand(mouse_x, mouse_y, obj_mO_bld_prd_furnanceCoal);
if (keyboard_check_released(ord("N"))) __debug_createSMOByHand(mouse_x, mouse_y, obj_mO_bld_prd_furnanceStone);
	

if (mouse_check_button(mb_middle)) {
	
	if (!position_meeting(mouse_x, mouse_y, obj_mO_bld_assemblyLine)) {	
		sMO_create(mouse_x, mouse_y, obj_mO_bld_assemblyLine, handlerCH_getChunkAtCoord(mouse_x, mouse_y), 0 ,bioms.grassland1);
	}
}

if (mouse_check_button_released(mb_left)) {
	__debug_createSMOByHand(mouse_x, mouse_y, obj_mO_bld_prd_mineCoal); // TEST
}


if (keyboard_check_released(vk_f10)){
	drawEnable = !drawEnable;
	draw_enable_drawevent(drawEnable);	
}

if (keyboard_check_released(vk_f11)) {
	window_set_fullscreen(!window_get_fullscreen());
}

if (keyboard_check_released(vk_f12)){
	global.debug = !global.debug;	
	show_debug_overlay(global.debug);
	layer_set_visible(COLLISION_LAYER, global.debug);
}

if (keyboard_check_released(vk_f9)) {
	benchmark = !benchmark;	
	
	if (!benchmark) {		
		benchmark_i = 0;
		benchmark_fps_real = 0;
		benchmark_fps = 0;
	}
}

if (benchmark) {
	benchmark_i ++;
	benchmark_fps_real += fps_real;
	benchmark_fps += fps;
}

if (keyboard_check_released(ord("R"))) {
	game_restart();
}

if (keyboard_check_released(ord("P"))) {
	inventory_changeItemPosition(global.inventory, 0, 0, 4, 4);
}

if (keyboard_check_released(ord("I"))) {
	inventory_addItem(global.inventory, item_Id.iron, 250);
}


if (keyboard_check_released(ord("O"))) {
	inventory_removeItem(global.inventory, item_Id.iron, 5);
}


