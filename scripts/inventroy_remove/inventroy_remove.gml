/// @func inventroy_removeItem
/// @param item
/// @param number
/// @desc removes some elements of that item
/// @return 

with (obj_logic_inventory) {
	
	var _invPosition = __inventory_findItem(argument0);	
	
	if (_invPosition[0] >= 0 ) {
		inv_grid[# _invPosition[0] , _invPosition[1]].itemNumber -= argument1;	
		
		// if there is no more element of that item we remove the item from that slot
		if (inv_grid[# _invPosition[0] , _invPosition[1]].itemNumber <=0) {
			instance_destroy(inv_grid[# _invPosition[0] , _invPosition[1]]);	
			inv_grid[# _invPosition[0] , _invPosition[1]] = noone;
		}
	}
	
}