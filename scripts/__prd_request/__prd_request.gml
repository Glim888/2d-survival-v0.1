/// @func __prd_request
/// @param 
/// @desc request prd_inputItems from connected "Production" Objects
/// @return 

alarm[1] = room_speed + random_range(-3, 3); // @TODO alarm timer

for (var _i=0; _i<prd_inputItemLen; _i++) {
	if (inventory_getItemNumber(prd_items, prd_inputItem[_i]) + prd_openRequests[_i] < INV_MAX) {
		if (logistics_requestItem(prd_logistics, prd_inputItem[_i])) prd_openRequests[_i]++;	
	}
}

