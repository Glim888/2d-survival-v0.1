/// @func __logistics_destroy
/// @param 
/// @desc destroy logic
/// @return 

for(var _i=0; _i<ds_list_size(buildings); _i++) logistics_deregister(buildings[| _i], 0);
for(var _i=0; _i<ds_list_size(assemblyLines); _i++) logistics_deregister(assemblyLines[| _i], 0);