/// @func inventory_removeItem
/// @param inventoryId
/// @param itemID
/// @param number
/// @desc decrease the number of that item and destroy the item slot if there are no more elements of that item
/// @return success or not

with (argument0) {
	
	var _slots = __inventory_findItem(argument1);
	var _temp = argument2;
	
	for (var _i=0; _i<array_length_1d(_slots); _i+=2) {
		
		var _val = min (inv_grid[# _slots[_i] , _slots[_i+1]].itemNumber, _temp);
		_temp -= _val;
		inv_grid[# _slots[_i] , _slots[_i+1]].itemNumber -= _val;
		
		// if there is no more element of that item we remove the item from that slot
		if (inv_grid[# _slots[_i] , _slots[_i+1]].itemNumber <=0) {
			__inventory_destroySlot(_slots[_i], _slots[_i+1]);
		}
		

	}
	
	if (_temp <= 0) return true;			
	Log("inventory_removeItem - We removed more items, than available! Use inventory_getItemNumber() before remove!", _temp);
	return false;
}